import React from 'react'
import PropTypes from 'prop-types';
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

var dateStr = "";
var date = new Date();

class AddForm extends BaseContainer {
  constructor(props) {
    super(props);
    this.fromBodyRender = this.fromBodyRender.bind(this);
    this.fromSubmit = this.fromSubmit.bind(this);
    this.state = {
      dataState: '',
      arrArea: [],
      arrOnPrinter: [],
      arrOffPrinter: [],
      isLoading: true
    };
  }

  componentWillMount() {
    this.setState({
      title: "Lines"
    })
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    //alert(dateStr)
  }

  async componentDidMount() {
    try {
        //Assign the promise unresolved first then get the data using the json method. 
        await ApiService.fetchCcaPlants()
        .then(response => {
          this.setState({ 
            arrArea: response.data
          })
        })
        await ApiService.fetchCcaOnlinePrinters()
        .then(response => {
          this.setState({ 
            arrOnPrinter: response.data
          })
        })
        await ApiService.fetchCcaOfflinePrinters()
        .then(response => {
          this.setState({ 
            arrOffPrinter: response.data
          })
        })
    } catch(err) {
        console.log("Error fetching data-----------", err);
    }
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    const item = {
      lineName: "",
      lineCode: "",
      recCreated: dateStr,
      recDatetime: dateStr,
      plantArea: 0,
      onLinePrinter: 1,
      offLinePrinter: 1,
      isRepack: "",
      ssccCode: ""
    };

    return <DataForm
        title="Add Lines"
        hasCancel={true}
        item={item}
        bodyRender={this.fromBodyRender}
        onSubmit={itm => { console.log(itm);
          // insert cca_lines
          ApiService.addCcaLines(itm)
          .then(res => {
            if(!res) throw new Error(res.status);
            else 
              this.fromSubmit(itm);
              //console.log(JSON.stringify(itm.plantId));
              alertMessage({message: 'Data '+itm.lineName+' added successfully.'})
          })
        }}
        onCancel={itm =>{

        }}
      />
  }

  fromSubmit(itm) {
    // select cca_plants get last lineId
    var isRepack = ''+itm.isRepack;
    var ssccCode = ''+itm.ssccCode;
    var plantId = parseInt(itm.plantArea);
    ApiService.fetchCcaLinesLast()
    .then(res => {
      res.data.map(val => {
        // check and insert cca_plant_line
        //ApiService.deleteCcaPlantLines(val.lineId);
        ApiService.addCcaPlantLines({
          "plantId": plantId,
          "lineId": val.lineId
        });

        // check and insert cca_line_repack
        //ApiService.deleteCcaLineRepacks(val.lineId);
        ApiService.addCcaLineRepacks({
          "lineId": val.lineId,
          "isRepack": isRepack,
          "ssccCode": ssccCode
        });
        console.log(JSON.stringify(val));
      });

    }); 
  }

  fromBodyRender(options) {
    //const plantArea = arrArea;
    const selArea = [];
    {this.state.arrArea.map(item => {
      selArea.push({title: item.plantDescr, value: item.plantId})
    })}
    const selOnPrinter = [];
    {this.state.arrOnPrinter.map(item => {
      selOnPrinter.push({title: item.printerName, value: item.printerId})
    })}
    const selOffPrinter = [];
    {this.state.arrOffPrinter.map(item => {
      selOffPrinter.push({title: item.printerName, value: item.printerId})
    })}
    const {item, binding} = options
    return <div className="row">
      <div className="col-md-8">
        <Select
          placeHolder="Plant Area"
          options={selArea}
          {...binding("plantArea")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
        placeHolder="Line Name"
            helpText= "Line Name"
            {...binding("lineName")}
          />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Line Code"
          helpText= "Line Code"
          {...binding("lineCode")}
        />
      </div>
      <div className="col-md-8">
        <Select
          placeHolder="Online Printer"
          options={selOnPrinter}
          {...binding("onLinePrinter")}
        />
      </div>
      <div className="col-md-8">
        <Select
          placeHolder="Offline Printer"
          options={selOffPrinter}
          {...binding("offLinePrinter")}
        />
      </div>
      <div className="col-sm-8">
        <CheckBox
          placeHolder="Repack"
          {...binding("isRepack")}
        />
      </div>
      <div className="col-xs-6 col-md-2">
        <TextInput
          placeHolder="Repack SSCC Code"
          helpText= "Number"
          {...binding("ssccCode")}
        />
      </div>
    </div>
  }
}

export default AddForm
