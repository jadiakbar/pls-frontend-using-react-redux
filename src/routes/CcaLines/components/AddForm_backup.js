import React from 'react'
import PropTypes from 'prop-types';
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

const TABS_DATA = []
var dateStr = "";
var date = new Date();
var plant_id = "";
var plant_name = "";

/* const namax = [
  { "title": "CCAI Cikedokan Plant", "value": 2 },
  { "title": "CCAI Test Plant", "value": 4 },
  { "title": "CCAI Semarang Plant", "value": 5 },
  { "title": "CCAI Bandung Plant", "value": 6 }
]; */
var arrArea = [];
ApiService.fetchCcaPlants()
.then(response => {
  response.data.map(data => {
    arrArea.push({title: data.plantDescr, value: data.plantId})
  }); 
})
console.log(arrArea);

class AddForm extends BaseContainer {

  componentWillMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2); 
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    const item = {
      lineName: "",
      lineCode: "",
      server: "",
      recCreated: dateStr,
      recDatetime: dateStr,
      plantArea: 4
    };

    return <DataForm
        title="Add Lines"
        hasCancel={true}
        item={item}
        bodyRender={this.fromBodyRender}
        onSubmit={itm => {
          ApiService.addCcaLines(itm)
            .then(res => {
              //this.props.history.push(View)
            })
            alertMessage({message: 'Data added successfully.'})
        }}
        onCancel={itm =>{

        }}
      />
  }

  fromBodyRender(options) {
    //const plantArea = arrArea;
    const {item, binding} = options
    return <div className="row">
      <div className="col-md-8">
        <Select
          placeHolder="Plant Area"
          options={arrArea}
          {...binding("plantArea")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
        placeHolder="Line Name"
            helpText= "Line Name"
            {...binding("lineCode")}
          />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="xxxx"
          helpText= "date"
          {...binding("recCreated")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Line Code"
          helpText= "Line Code"
          {...binding("lineCode")}
        />
      </div>
      <div className="col-md-8">
        <Select
          placeHolder="Online Printer"
          options={[
            {title:"Online 1", value: "O1"},
            {title:"Online 2", value: "O2"},
            {title:"Online 3", value: "O3"}]}
          {...binding("printer")}
        />
      </div>
      <div className="col-md-8">
        <Select
          placeHolder="Offline Printer"
          options={[
            {title:"OFF 1", value: "F1"},
            {title:"OFF 2", value: "F2"},
            {title:"OFF 3", value: "F3"}]}
          {...binding("server")}
        />
      </div>
      <div className="col-sm-8">
        <CheckBox
          placeHolder="Repack"
          {...binding("repack")}
        />
      </div>
    </div>
  }
}

export default AddForm
