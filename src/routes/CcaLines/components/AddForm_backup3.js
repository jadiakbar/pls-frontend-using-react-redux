import React from 'react'
import PropTypes from 'prop-types';
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

var dateStr = "";
var date = new Date();
var plant_id = "";
var plant_name = "";

var arrArea = [];
ApiService.fetchCcaPlants()
.then(response => {
  response.data.map(data => {
    arrArea.push({title: data.plantDescr, value: data.plantId})
  }); 
});

var arrOnPrinter = [];
ApiService.fetchCcaOnlinePrinters()
.then(response => {
  response.data.map(data => {
    arrOnPrinter.push({title: data.printerName, value: data.printerId})
  }); 
});

var arrOffPrinter = [];
ApiService.fetchCcaOfflinePrinters()
.then(response => {
  response.data.map(data => {
    arrOffPrinter.push({title: data.printerName, value: data.printerId})
  }); 
});

class AddForm extends BaseContainer {
  constructor(props) {
    super(props);
    this.fromBodyRender = this.fromBodyRender.bind(this);
    this.fromSubmit = this.fromSubmit.bind(this);
    this.state = {
      isLoading: true
    };
  }

  componentWillMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2); 
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    const item = {
      lineName: "",
      lineCode: "",
      recCreated: dateStr,
      recDatetime: dateStr,
      plantArea: 1,
      onLinePrinter: 1,
      offLinePrinter: 1,
      repack:0
    };

    return <DataForm
        title="Add Lines"
        hasCancel={true}
        item={item}
        bodyRender={this.fromBodyRender}
        onSubmit={itm => {
          // insert cca_lines
          ApiService.addCcaLines({
            "lineName": itm.lineName,
            "lineCode": itm.lineCode,
            "recCreated": itm.recCreated,
            "recDatetime": itm.recDatetime
          })
          .then(res => {
            //this.props.history.push(View)
            console.log(res);
          });

          // select cca_plants get last lineId
          var onLinePrinter = parseInt(itm.onLinePrinter);
          var offLinePrinter = parseInt(itm.offLinePrinter);
          ApiService.fetchCcaLinesLast()
          .then(res => {
            res.data.map(val => {
              // check and insert cca_plant_line
              ApiService.deleteCcaPlantLines(val.lineId);
              ApiService.addCcaPlantLines({
                "plantId": val.plantId,
                "lineId": val.lineId
              });

              // check and insert cca_line_printer
              ApiService.deleteCcaLinePrinters(val.lineId);
              ApiService.addCcaLinePrinters({
                "lineId": val.lineId,
                "printerId": onLinePrinter
              });

              // check and delete cca_line_scanner
              ApiService.deleteCcaLineScanners(val.lineId);
              console.log(JSON.stringify(val));
            });

          }); 
          console.log(JSON.stringify('ini plantArea ='+itm.plantArea+' ini onLinePrinter='+itm.onLinePrinter+' ini offLinePrinter='+itm.onLinePrinter));
          //console.log(JSON.stringify(itm.plantId));
          alertMessage({message: 'Data '+itm.lineName+' added successfully.'})
        }}
        onCancel={itm =>{

        }}
      />
  }

  fromBodyRender(options) {
    //const plantArea = arrArea;
    const {item, binding} = options
    return <div className="row">
      <div className="col-md-8">
        <Select
          placeHolder="Plant Area"
          options={arrArea}
          {...binding("plantArea")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
        placeHolder="Line Name"
            helpText= "Line Name"
            {...binding("lineName")}
          />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Line Code"
          helpText= "Line Code"
          {...binding("lineCode")}
        />
      </div>
      <div className="col-md-8">
        <Select
          placeHolder="Online Printer"
          options={arrOnPrinter}
          {...binding("onLinePrinter")}
        />
      </div>
      <div className="col-md-8">
        <Select
          placeHolder="Offline Printer"
          options={arrOffPrinter}
          {...binding("offLinePrinter")}
        />
      </div>
      <div className="col-sm-8">
        <CheckBox
          placeHolder="Repack"
          {...binding("repack")}
        />
      </div>
    </div>
  }
}

export default AddForm
