/**
*
* Auto Generated file
*
*/

import Tab from './Tab'
import Table from './Table'
import CounterRoute from './Counter'
import BlankTemp from './BlankTemp'
import SampleCRUD from './SampleCRUD'
//{append_include_here}
import CcaPlants from './CcaPlants'
import CcaLines from './CcaLines'
import CcaPrinters from './CcaPrinters'
import CcaScanners from './CcaScanners'
import CcaItems from './CcaItems'
import CcaProcessOrders from './CcaProcessOrders'
//import MasterRoles from './MasterRoles'
//import MasterUsers from './MasterUsers'
import CcaDevices from './CcaDevices'
import CcaSettings from './CcaSettings'
import ManualPrinting from './ManualPrinting'
import PartialPalletPrinting from './PartialPalletPrinting'
import RepackPrinting from './RepackPrinting'
import ReworkPrinting from './ReworkPrinting'
import PalleteReport from './PalleteReport'
import Lpa from './Lpa'

export default [
  CounterRoute,
  Tab,
  Table,
  BlankTemp,
  SampleCRUD,
  //{append_module_here}
  CcaPlants,
  CcaLines,
  CcaPrinters,
  CcaScanners,
  CcaItems,
  CcaProcessOrders,
  //MasterRoles,
  //MasterUsers,
  CcaDevices,
  CcaSettings,
  ManualPrinting,
  PartialPalletPrinting,
  RepackPrinting,
  ReworkPrinting,
  PalleteReport,
  Lpa
]
