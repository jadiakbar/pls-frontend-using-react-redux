import React from 'react'
import PropTypes from 'prop-types';
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

const TABS_DATA = []
var dateStr = "";
var date = new Date();
var plant_id = "";
var plant_name = "";

var arrArea = [];
ApiService.fetchCcaPlants()
.then(response => {
  response.data.map(data => {
    arrArea.push({title: data.plantDescr, value: data.plantId})
  }); 
});

var arrOnPrinter = [];
ApiService.fetchCcaOnlinePrinters()
.then(response => {
  response.data.map(data => {
    arrOnPrinter.push({title: data.printerName, value: data.printerId})
  }); 
});

var arrOffPrinter = [];
ApiService.fetchCcaOfflinePrinters()
.then(response => {
  response.data.map(data => {
    arrOffPrinter.push({title: data.printerName, value: data.printerId})
  }); 
});

class AddForm extends BaseContainer {

  componentWillMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2); 
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    const item = {
      lineName: "",
      lineCode: "",
      recCreated: dateStr,
      recDatetime: dateStr,
      plantArea: 1,
      onLinePrinter: 1,
      offLinePrinter: 1,
      repack:0
    };

    return <DataForm
        title="Manual Printing"
        hasCancel={true}
        item={item}
        bodyRender={this.fromBodyRender}
        onSubmit={itm => {
          // insert cca_lines
          ApiService.addCcaLines({
            "lineName": itm.lineName,
            "lineCode": itm.lineCode,
            "recCreated": itm.recCreated,
            "recDatetime": itm.recDatetime
          })
          .then(res => {
            //this.props.history.push(View)
            console.log(res);
          });

          // select cca_plants get last lineId
          var onLinePrinter = parseInt(itm.onLinePrinter);
          var offLinePrinter = parseInt(itm.offLinePrinter);
          ApiService.fetchCcaLinesLast()
          .then(res => {
            res.data.map(val => {
              // check and insert cca_plant_line
              ApiService.deleteCcaPlantLines(val.lineId);
              ApiService.addCcaPlantLines({
                "plantId": val.plantId,
                "lineId": val.lineId
              });

              // check and insert cca_line_printer
              ApiService.deleteCcaLinePrinters(val.lineId);
              ApiService.addCcaLinePrinters({
                "lineId": val.lineId,
                "printerId": onLinePrinter
              });

              // check and delete cca_line_scanner
              ApiService.deleteCcaLineScanners(val.lineId);
              console.log(JSON.stringify(val));
            });

          }); 
          console.log(JSON.stringify('ini plantArea ='+itm.plantArea+' ini onLinePrinter='+itm.onLinePrinter+' ini offLinePrinter='+itm.onLinePrinter));
          //console.log(JSON.stringify(itm.plantId));
          alertMessage({message: 'Data '+itm.lineName+' added successfully.'})
        }}
        onCancel={itm =>{

        }}
      />
  }

  fromBodyRender(options) {
    //const plantArea = arrArea;
    const {item, binding} = options
    return <div className="row">
      <div className="col-md-8">
        <Select
          placeHolder="Line Number"
          options={arrArea}
          {...binding("plantArea")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
        placeHolder="Production Order"
            helpText= "Production Order"
            {...binding("lineName")}
          />
      </div>
      <div className="col-md-8">
        <Select
          placeHolder="Material Number"
          options={arrArea}
          {...binding("plantArea")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
        placeHolder="Material Description"
            helpText= "Material Description"
            {...binding("lineName")}
          />
      </div>
      <div className="col-md-8">
        <TextInput
        placeHolder="Best Before Date"
            helpText= "Best Before Date"
            {...binding("lineName")}
          />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Qty per Pallet"
          helpText= "Qty per Pallet"
          {...binding("lineCode")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Number of Labels"
          helpText= "Number of Labels"
          {...binding("lineCode")}
        />
      </div>
      <div className="col-md-8">
        <Select
          placeHolder="Offline Printer"
          options={arrOnPrinter}
          {...binding("onLinePrinter")}
        />
      </div>  
    </div>
  }
}

export default AddForm
