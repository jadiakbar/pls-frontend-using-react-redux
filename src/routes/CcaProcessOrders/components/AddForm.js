import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

const TABS_DATA = []
var dateStr = "";
var date = new Date();

class AddForm extends BaseContainer {
  handleSubmitted = ({ res, fields, form }) => {
    form.reset() // resets view
  }

  componentWillMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    //alert(dateStr)
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    const item = {
      plantDescr: "",
      plantCode: "",
      server: "",
      recCreated: dateStr,
      recDatetime: dateStr
    }

    return <DataForm
        title="Add Plants"
        hasCancel={true}
        item={item}
        bodyRender={this.fromBodyRender}
        onSubmit={itm => {
          ApiService.addCcaPlants(itm)
            .then(res => {
              //this.props.history.push(View)
            })
            alertMessage({message: 'Data added successfully.'})
        }}
        onCancel={itm =>{

        }}
      />
  }

  fromBodyRender(options) {
    const {item, binding} = options
    return <div className="row">
      <div className="col-xs-3 col-md-5">
        <TextInput
        placeHolder="Plant Name"
            helpText= ""
            {...binding("plantDescr")}
          />
      </div>
      <div className="col-xs-3 col-md-5">
        <TextInput
          placeHolder="Plant Code"
          helpText= ""
          {...binding("plantCode")}
        />
      </div>
      <div className="col-sm-4">
        <CheckBox
          placeHolder="GR does not use real-time data"
          {...binding("remember1")}
        />
      </div>
      <div className="col-xs-6 col-md-2">
        <TextInput
          placeHolder="Second(s)"
          helpText= ""
          {...binding("")}
        />
      </div>
      <div className="col-sm-8">
        <CheckBox
          placeHolder="Using EWM System"
          {...binding("remember2")}
        />
      </div>
    </div>
  }
}

export default AddForm
