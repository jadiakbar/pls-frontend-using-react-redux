import React, { Component } from 'react';
import ApiService from '../../../service/ApiService'
import {alertMessage} from '../../../utils'
import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

var dateStr = "";
var date = new Date();
var key_id = 0;

export default class Edit extends Component {
  constructor(props) {
    super(props);
    this.onChangeScannerName = this.onChangeScannerName.bind(this);
    this.onChangeScannerIP = this.onChangeScannerIP.bind(this);
    this.onChangeScannerPORT = this.onChangeScannerPORT.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      scannerId: '',
      scannerName: '',
      scannerIp:'',
      scannerPort: ''
    }
  }

  componentDidMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);

    var url = window.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    key_id = idVal;

    ApiService.fetchCcaScannersById(key_id)
      .then(response => {
        this.setState({ 
          scannerId: response.data.scannerId,
          scannerName: response.data.scannerName, 
          scannerIp: response.data.scannerIp,
          scannerPort: response.data.scannerPort
        });
      })
      .catch(function (error) {
        console.log(error);
      })
    }

  onChangeScannerName(e) {
    this.setState({
      scannerName: e.target.value
    });
  }
  onChangeScannerIP(e) {
    this.setState({
      scannerIp: e.target.value
    })  
  }
  onChangeScannerPORT(e) {
    this.setState({
      scannerPort: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();
    var paramsId = parseInt(key_id, 10);  // 10 Decimal
    const obj = {
      scannerId: paramsId,//this.props.match.params.id,
      scannerName: this.state.scannerName,
      scannerIp: this.state.scannerIp,
      scannerPort: this.state.scannerPort,
      recCreated: dateStr,
      recDatetime: dateStr
    };
    console.log(obj);
    ApiService.editCcaScanners(paramsId,obj)
    .then(res => {
      //this.props.history.push(View)
      this.props.history.push("/View")
    })
    alertMessage({message: 'Data added successfully.'})
  }
 
  render() {
    return (
        <div className="portlet light bordered">
          <div className="portlet-title">
            <Link to="/View" className="btn sbold blue" id="btnView">VIEW DATA<i className="fa fa-list-alt "></i></Link>
            <br />
            <div className="caption font-red-sunglo">
              <i className="icon-settings font-red-sunglo"></i><span className="caption-subject bold uppercase">Edit Printers</span>
            </div>
          </div>
          <div className="portlet-body form">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <div className="row">
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Scanner Name :  </label>
                      <input 
                        type="text" 
                        className="form-control" 
                        value={this.state.scannerName}
                        onChange={this.onChangeScannerName}
                        />
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Scanner IP : </label>
                      <input type="text" 
                        className="form-control"
                        value={this.state.scannerIp}
                        onChange={this.onChangeScannerIP}
                        />
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Scanner Port : </label>
                      <input type="text" 
                        className="form-control"
                        value={this.state.scannerPort}
                        onChange={this.onChangeScannerPORT}
                        />
                    </div>
                  </div>
                  <div className="col-sm-12">
                    <div className="form-actions noborder">
                      <input type="submit" 
                        value="Update" 
                        className="btn blue"/>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
    )
  }
}