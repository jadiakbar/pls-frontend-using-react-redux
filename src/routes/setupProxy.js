const proxy = require("http-proxy-middleware");

module.export = function(app) {
	app.use(
		proxy("/api/CcaPlants", {
			target : "http://localhost:5000",
			secure: false,
			changeOrigin: true,
			pathRewrite: {
		        "^/api/CcaPlants": "/api/CcaPlants"
		      }
		})
	);

	app.use(
		proxy("/api", {
			target : "http://localhost:5000",
			secure: false,
			changeOrigin: true,
			pathRewrite: {
		        "^/api": "/api"
		      }
		})
	);
};