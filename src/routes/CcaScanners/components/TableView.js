import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import Table from '../../../components/Table'
import ApiService from '../../../service/ApiService'
import {alertMessage} from '../../../utils'
import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';
import Edit from '../components/EditForm';


//const TABLE_DATA = []
var data = [];

class TableView extends BaseContainer {
  state={
    header: [{
      title:"#",
      prop: "no"
  }, {
    title:"Scanner Name",
    prop: "scannerName"
    }, {
      title:"Scanner IP",
      prop: "scannerIp"
    }, {
      title:"Scanner Port",
      prop:"scannerPort"
    }, {
      title:"Action"
    }],
    data: data
  }
  

  componentWillMount() {
    this.setState({
      title: "SICK Scanners"
    })
    ApiService.fetchCcaScanners()
      .then(response => {
        this.setState({ 
          data: response.data 
        });
      })
      this.showLoading(true)
        setTimeout(() => {
          this.showLoading(false)
        }, 500);
  }


  _renderContent() {
    console.log(this.state)
    return <div className="portlet body">
      <Table
        searchableProperties={["scannerName", "scannerIp","scannerPort"]}
        hasTopBar={true}
        headers={this.state.header}
        data={this.state.data}
        rowRender={this._renderRow}
        onAddNewHandler={this.onAddNewClick.bind(this)}
      />
    </div>
  }

  onAddNewClick() {
    /* const newData = this.state.data.slice(0)
    newData.push(
      {"no": "", "scannerName": "time_" + Date.now(), "": "Scanner IP", "scannerPort": "", "": "Repack", "": "RP SSCC Code"}
    )

    this.setState({
      data: newData
    }) */
  alert('Got to Add Data');
  }

  _renderRow(itm, i) {
    return <tr key={i} className="odd gradeX">
      <td>{i+1}</td>
      <td>{itm.scannerName}</td>
      <td>{itm.scannerIp}</td>
      <td>{itm.scannerPort}</td>
      <td>
        <div className="btn-group">
          <button className="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
            <i className="fa fa-angle-down"></i>
          </button>
          <ul className="dropdown-menu pull-left" role="menu">
            <li>
              <a href="javascript:;">
                <i className="icon-flag"></i> Area Name
                <span className="badge badge-success">4</span>
              </a>
            </li>
            <li>
              <a href="javascript:;">
                <i className="glyphicon glyphicon-tags"></i> Area Code </a>
            </li>
            <li className="divider"> </li>
            <li>
              <Link to={'/Edit/Find?id='+itm.scannerId } id="btnEdit"><i className="glyphicon glyphicon-pencil"></i> Edit </Link>
            </li>
            <li>
              <a onClick={e => handleClick(itm.scannerId)} >
                <i className="glyphicon glyphicon-trash"></i> Delete </a>
            </li>
          </ul>
        </div>
      </td>
    </tr>
  }
}

function handleClick(val) {
  //alert(e)
  var result = confirm("Are you sure...?");
  if (result) {
    ApiService.deleteCcaSickLineScanners(val);
    ApiService.deleteCcaScanners(val)
    .then(response => {
      this.props.history.push("/View");
      alertMessage({message: 'Data '+ response.data.scannerName +' deleted successfully'})
    })
  }
}

export default TableView
