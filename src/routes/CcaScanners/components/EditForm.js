import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

const TABS_DATA = []
var dateStr = "";
var date = new Date();
var key_id = "";

class EditForm extends BaseContainer {
  constructor(props) {
    super(props);
    this.fromBodyRender = this.fromBodyRender.bind(this);
    this.fromSubmit = this.fromSubmit.bind(this);
    this.state = {
        dataState: null,
        arrLines: [],
        scannerId: '',
        scannerName: '',
        scannerIp: '',
        scannerPort: 0,
        recCreated: '',
        recDatetime: '',
        arrLineId: null,
        lineId: 0,
        isLoading: true
    }
  }

  componentWillMount() {
    this.setState({
      title: "SICK Scanners"
    })
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);

    var url = window.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    key_id = idVal;
  }

  async componentDidMount() {
    try {
        //Assign the promise unresolved first then get the data using the json method. 
        await ApiService.fetchCcaLines()
        .then(response => {
          this.setState({ 
            arrLines: response.data
          })
        })
        //console.log(this.state.arrLines);

        await ApiService.fetchCcaLinesById(key_id)
        .then(response => {
          this.setState({ 
            arrLineId: response.data
          })
        })
        //console.log(this.state.arrLineId);
        if(this.state.arrLineId !== null){
          {this.state.arrLineId.map(item => {
            this.setState({ 
              lineId: `${item.lineId}`
            })
          })}
        }

        await ApiService.fetchCcaScannersById(key_id)
        .then(response => {
          this.setState({ 
            dataState: response.data
          })
        })
        //console.log(this.state.dataState);
        if(this.state.dataState !== null){
          {this.state.dataState.map(item => {
            this.setState({ 
              scannerId: `${item.scannerId}`,
              scannerName: `${item.scannerName}`,
              scannerIp: `${item.scannerIp}`,
              scannerPort: `${item.scannerPort}`,
              recCreated: dateStr,
              recCreated: dateStr
            })
          })}
        }
    } catch(err) {
        console.log("Error fetching data-----------", err);
    }
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    if(this.state.scannerName) {
      var paramsId = parseInt(key_id, 10);  // 10 Decimal
      var lineId = parseInt(this.state.lineId);
      console.log(this.state.scannerName);
      const item = {
        scannerId: paramsId,
        scannerName: this.state.scannerName,
        scannerIp: this.state.scannerIp,
        scannerPort: this.state.scannerPort,
        recCreated: dateStr,
        recDatetime: dateStr,
        lineId: lineId
      }

      return <DataForm
          title="Add Scanners"
          hasCancel={true}
          item={item}
          bodyRender={this.fromBodyRender}
          onSubmit={itm => {  console.log(itm);
            var scannerPort = parseInt(itm.scannerPort);
            ApiService.editCcaScanners(paramsId,{
              scannerId: paramsId,
              scannerName: itm.scannerName,
              scannerIp: itm.scannerIp,
              scannerPort: scannerPort,
              recCreated: itm.recCreated,
              recDatetime: itm.recDatetime
            })
              .then(res => {
                if(!res) throw new Error(res.status);
                else 
                  this.fromSubmit(itm);
                  //console.log(JSON.stringify(itm.plantId));
                  this.props.history.push("/View");
                  alertMessage({message: 'Data '+itm.scannerName+' update successfully.'})
              })
          }}
          onCancel={itm =>{
              
          }}
        />
    }
  }

  fromSubmit(itm) {
    console.log(itm);
    var lineId = parseInt(itm.lineId);
    var scannerId = parseInt(itm.scannerId);
    ApiService.deleteCcaSickLineScanners(scannerId);
    ApiService.addCcaLineScanners({
      "lineId": lineId,
      "scannerId": scannerId
    });
  }

  fromBodyRender(options) {
    const selLines = [];
    {this.state.arrLines.map(item => {
      selLines.push({title: item.lineName, value: item.lineId})
    })}
    const {item, binding} = options
    return <div className="row">
      <div className="col-md-8">
        <TextInput
        placeHolder="Scanner Name"
            helpText= "Scanner Name"
            {...binding("scannerName")}
          />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Scanner IP"
          helpText= "Scanner IP"
          {...binding("scannerIp")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Scanner Port"
          helpText= "Scanner Port"
          {...binding("scannerPort")}
        />
      </div>
      <div className="col-md-8">
        <Select
          placeHolder="Line"
          options={selLines}
          {...binding("lineId")}
        />
      </div>
    </div>
  }
}

export default EditForm
