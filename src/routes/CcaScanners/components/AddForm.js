import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

const TABS_DATA = []
var dateStr = "";
var date = new Date();
//var lineId = "";
//var lineCode = "";
//const data = [];

class AddForm extends BaseContainer {
  constructor(props) {
    super(props);
    this.fromBodyRender = this.fromBodyRender.bind(this);
    this.fromSubmit = this.fromSubmit.bind(this);
    this.state = {
        dataState: null,
        arrLines: [],
        isLoading: true
    }
  }

  componentWillMount() {
    this.setState({
      title: "SICK Scanners"
    })
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
  }

  async componentDidMount() {
    try {
        //Assign the promise unresolved first then get the data using the json method. 
        await ApiService.fetchCcaLines()
        .then(response => {
          this.setState({ 
            arrLines: response.data
          })
        })
    } catch(err) {
        console.log("Error fetching data-----------", err);
    }
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    const item = {
      scannerName: "",
      scannerIp: "",
      scannerPort: 0,
      recCreated: dateStr,
      recDatetime: dateStr,
      lineId: ''
    }

    return <DataForm
        title="Add Scanners"
        hasCancel={true}
        item={item}
        bodyRender={this.fromBodyRender}
        onSubmit={itm => {  console.log(itm);
          var scannerPortVal = parseInt(itm.scannerPort);
          ApiService.addCcaScanners({
            scannerName: itm.scannerName,
            scannerIp: itm.scannerIp,
            scannerPort: scannerPortVal,
            recCreated: itm.recCreated,
            recDatetime: itm.recDatetime
          })
            .then(res => {
              if(!res) throw new Error(res.status);
              else 
                this.fromSubmit(itm);
                //console.log(JSON.stringify(itm.plantId));
                alertMessage({message: 'Data '+itm.scannerName+' added successfully.'})
            })
        }}
        onCancel={itm =>{
            
        }}
      />
  }

  fromSubmit(itm) {
    console.log(itm);
    var lineId = parseInt(itm.lineId);
    ApiService.fetchCcaScannerLast()
    .then(res => {
      res.data.map(val => {
        // check and insert cca_line_scanners
        ApiService.addCcaLineScanners({
          "lineId": lineId,
          "scannerId": val.scannerId
        });
        console.log(JSON.stringify(val));
      });

    }); 
  }

  fromBodyRender(options) {
    const selLines = [];
    {this.state.arrLines.map(item => {
      selLines.push({title: item.lineName, value: item.lineId})
    })}
    const {item, binding} = options
    return <div className="row">
      <div className="col-md-8">
        <TextInput
        placeHolder="Scanner Name"
            helpText= "Scanner Name"
            {...binding("scannerName")}
          />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Scanner IP"
          helpText= "Scanner IP"
          {...binding("scannerIp")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Scanner Port"
          helpText= "Scanner Port"
          {...binding("scannerPort")}
        />
      </div>
      <div className="col-md-8">
        <Select
          placeHolder="Line"
          options={selLines}
          {...binding("lineId")}
        />
      </div>
    </div>
  }
}

export default AddForm
