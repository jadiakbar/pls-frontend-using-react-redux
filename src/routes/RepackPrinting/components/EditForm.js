import React, { Component } from 'react';
import ApiService from '../../../service/ApiService'
import {alertMessage} from '../../../utils'
import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

var dateStr = "";
var date = new Date();
var key_id = 0;

export default class EditForm extends Component {
  constructor(props) {
    super(props);
    this.onChangePlantArea = this.onChangePlantArea.bind(this);
    this.onChangeLineName = this.onChangeLineName.bind(this);
    this.onChangeLineCode = this.onChangeLineCode.bind(this);
    /* this.onChangeOnlinePrinter = this.onChangeOnlinePrinter.bind(this);
    this.onChangeOffLinePrinter = this.onChangeOffLinePrinter.bind(this);
    this.onChangeRepack = this.onChangeRepack.bind(this);
    this.onChangeRepackSSCCCode = this.onChangeRepackSSCCCode.bind(this); */
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      plantId: '',
      plantDescr: '',
      plantCode: '',
      plantArea: '',
      lineId: '',
      lineCode: '',
      lineName: ''
    }
  }

  componentDidMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);

    var url = this.props.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    key_id = idVal;

    ApiService.fetchCcaLinesById(key_id)
      .then(response => {
        this.setState({ 
          plantId: response.data.plantId,
          plantDescr: response.data.plantDescr, 
          plantCode: response.data.plantCode,
          plantArea: response.data.plantArea,
          lineId: response.data.lineId,
          lineCode: response.data.lineCode,
          lineName: response.data.lineName
        });
        console.log(response.data)
      })
      .catch(function (error) {
        console.log(error);
      })
    }

  onChangePlantArea(e) {
    this.setState({
      plantArea: e.target.value
    });
  }
  onChangeLineName(e) {
    this.setState({
      lineName: e.target.value
    })  
  }
  onChangeLineCode(e) {
    this.setState({
      lineCode: e.target.value
    })
  }
  /* onChangeOnlinePrinter(e) {
    this.setState({
      //lineCode: e.target.value
    })
  }
  onChangeOffLinePrinter(e) {
    this.setState({
      //lineCode: e.target.value
    })
  }
  onChangeRepack(e) {
    this.setState({
      //lineCode: e.target.value
    })
  }
  onChangeRepackSSCCCode(e) {
    this.setState({
      //lineCode: e.target.value
    })
  } */

  onSubmit(e) {
    e.preventDefault();
    var paramsId = parseInt(key_id, 10);  // 10 Decimal
    const obj = {
      plantId: this.state.plantId,
      plantDescr: this.state.plantDescr,
      plantCode: this.state.plantCode,
      plantArea: this.state.plantArea,
      lineId: paramsId,//this.props.match.params.id,
      lineName: this.state.lineName,
      lineCode: this.state.lineCode,
      recCreated: dateStr,
      recDatetime: dateStr
    };
    console.log(obj);
    ApiService.editCcaLines(paramsId,obj)
    .then(res => {
      //this.props.history.push(View)
      this.props.history.push("/View")
    })
    alertMessage({message: 'Data update successfully.'})
  }
 
  render() {
    return (
        <div className="portlet light bordered">
          <div className="portlet-title">
            <Link to="/View" className="btn sbold blue" id="btnView">VIEW DATA<i className="fa fa-list-alt "></i></Link>
            <br />
            <div className="caption font-red-sunglo">
              <i className="icon-settings font-red-sunglo"></i><span className="caption-subject bold uppercase">Edit Lines</span>
            </div>
          </div>
          <div className="portlet-body form">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <div className="row">
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Plant Area :  </label>
                      <input 
                        type="text" 
                        className="form-control" 
                        value={this.state.plantArea}
                        onChange={this.onChangePlantArea}
                        />
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Line Name : </label>
                      <input type="text" 
                        className="form-control"
                        value={this.state.lineName}
                        onChange={this.onChangeLineName}
                        />
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Line Code : </label>
                      <input type="text" 
                        className="form-control"
                        value={this.state.lineCode}
                        onChange={this.onChangeLineCode}
                        />
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Online Printer : </label>
                      <input type="text" 
                        className="form-control"
                        value=""
                        onChange=""
                        />
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Offline Printer : </label>
                      <input type="text" 
                        className="form-control"
                        value=""
                        onChange=""
                        />
                    </div>
                  </div>
                  <div className="col-sm-12">
                    <div className="md-checkbox">
                      <input type="checkbox" className="form-check-input" id="chkEwm" />
                      <label htmlFor="chkEwm"><span className="inc"></span><span className="check"></span><span className="box"></span> Repack</label>
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Offline Printer : </label>
                      <input type="text" 
                        className="form-control"
                        value=""
                        onChange=""
                        />
                    </div>
                  </div>
                  <div className="col-sm-12">
                    <div className="form-actions noborder">
                      <input type="submit" 
                        value="Update" 
                        className="btn blue"/>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
    )
  }
}