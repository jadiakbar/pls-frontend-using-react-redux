import React, { Component } from 'react';
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

//const TABS_DATA = []
var dateStr = "";
var date = new Date();

class EditForm extends BaseContainer {
  /* state={
      dataState: null,
      isLoading: true
    } */
  constructor(props) {
    super(props);
    this.fromBodyRender = this.fromBodyRender.bind(this);
    this.state = { 
      data: null,
      loading: true
    };
  }


  async componentDidMount() {
  //componentWillMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    //alert(dateStr)
    
    var url = window.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    var key_id = idVal;
    console.log(key_id);
     try {
        //Assign the promise unresolved first then get the data using the json method. 
        ApiService.fetchGetCcaPlantbyId(key_id)
        .then(response => {
          this.setState({ 
            data: response.data,
            loading: false
          })
        })
    } catch(err) {
        console.log("Error fetching data-----------", err);
    } 
    

    /* try {
        //Assign the promise unresolved first then get the data using the json method. 
        await ApiService.fetchGetCcaPlantbyId(key_id)
        .then(response => {
          this.setState({ 
            dataState: response.data,
            loading: false
          })
        })
    } catch(err) {
        console.log("Error fetching data-----------", err);
    } */
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    //let data = this.state.dataState;
    //const { isLoading, isLoaded, data } = this.props;
    //if (isLoading) return <Loader />;
    console.log(this.state.data);
    if(this.state.data !== null){
      {this.state.data.map(data => {
        let item = { plantDescr: "Test akbar 2", plantCode: "6677", grInterval: "600", isInterval: 1, useWms: 0, recCreated: "2020-03-10T21:51:18", recDatetime: "2020-03-10T21:51:18" }
        console.log(item)

        return <DataForm
          title="Edit Plants"
          hasCancel={true}
          item={item}
          bodyRender={this.fromBodyRender}
          onSubmit={itm => {
            // insert cca_plants
            ApiService.addCcaPlants(itm)
            .then(res => {
              //this.props.history.push(View)
            });

            // select cca_plants get last plantId
            var isInterval = ''+itm.isInterval;
            var useWms = ''+itm.useWms;
            var grInterval = parseInt(itm.grInterval);
            ApiService.fetchCcaPlantsLast()
            .then(res => {
              res.data.map(val => {
                // check and insert cca_plant_realtime
                ApiService.deleteCcaPlantRealtimes(val.plantId);
                ApiService.addCcaPlantRealtimes({
                  "plantId": val.plantId,
                  "isInterval": isInterval,
                  "grInterval": grInterval
                });

                // check and insert cca_plant_wms
                ApiService.deleteCcaPlantWms(val.plantId);
                ApiService.addCCcaPlantWms({
                  "plantId": val.plantId,
                  "useWms": useWms
                });
              });

            }); 
            //console.log(JSON.stringify(itm));
            //console.log(JSON.stringify(itm.plantId));
            alertMessage({message: 'Data '+itm.plantDescr+' added successfully.'})
          }}
          onCancel={itm =>{
            alert("Data reset");
          }}
        />
      })}
    }
  }

  fromBodyRender(options) {
    if(this.state.data !== null){
      const {item, binding} = options
      return <div className="row">
        <div className="col-xs-3 col-md-5">
          <TextInput
          placeHolder="Plant Name"
              helpText= "Plant Name"
              {...binding("plantDescr")}
            />
        </div>
        <div className="col-xs-3 col-md-5">
          <TextInput
            placeHolder="Plant Code"
            helpText= "Plant Code"
            {...binding("plantCode")}
          />
        </div>
        <div className="col-sm-4">
          <CheckBox
            placeHolder="GR does not use real-time data"
            {...binding("isInterval")}
          />
        </div>
        <div className="col-xs-6 col-md-2">
          <TextInput
            placeHolder="Second(s)"
            helpText= "Number"
            {...binding("grInterval")}
          />
        </div>
        <div className="col-sm-8">
          <CheckBox
            placeHolder="Using EWM System"
            {...binding("useWms")}
          />
        </div>
      </div>
    }
  }
}

export default EditForm
