
import React, { Component } from 'react';
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

var dateStr = "";
var date = new Date();
let plantDescr, plantCode, grInterval, isInterval, useWms, recCreated, recDatetime;
var items = [];

class EditForm extends BaseContainer {
  constructor(props) {
    super(props);
    //this.fromBodyRender = this.fromBodyRender.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = { 
      data: null,
      loading: true
    };
  }


  async componentDidMount() {
    //componentWillMount() {
    this.setState({
      title: "Edit Plant Area"
    })
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    //alert(dateStr)

    var url = window.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    var key_id = idVal;
    console.log(key_id);
     try {
        //Assign the promise unresolved first then get the data using the json method. 
        ApiService.fetchGetCcaPlantbyId(key_id)
        .then(response => {
          this.setState({ 
            data: response.data,
            loading: false
          }) 
        })
    } catch(err) {
        console.log("Error fetching data-----------", err);
    } 
  }

  onSubmit(e) {
    e.preventDefault();
    var paramsId = parseInt(key_id, 10);  // 10 Decimal
    const obj = {
      plantId: paramsId,//this.props.match.params.id,
      plantDescr: this.state.plantDescr,
      plantCode: this.state.plantCode,
      recCreated: dateStr,
      recDatetime: dateStr
    };
    console.log(obj);
    ApiService.editCcaPlants(paramsId,obj)
    .then(res => {
      //this.props.history.push(View)
      this.props.history.push("/View")
    })
    alertMessage({message: 'Data added successfully.'})
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    if(this.state.data !== null){
      {this.state.data.map(item => {
        plantDescr=`${item.plantDescr}`;
        plantCode=`${item.plantCode}`;
        grInterval=`${item.grInterval}`;
        isInterval=`${item.isInterval}`;
        if(`${item.isInterval}` == "1") {
          isInterval=true
        }else{
          isInterval=false
        }
        if(`${item.useWms}` == "1") {
          useWms=true
        }else{
          useWms=false
        }
        recCreated=dateStr;
        recDatetime=dateStr;
      })}
      
        return <div className="row">
          <DataForm
          title="Edit Plants"
          hasCancel={true}
          onSubmit={itm => {
            // insert cca_plants
            ApiService.addCcaPlants(itm)
            .then(res => {
              //this.props.history.push(View)
            });

            // select cca_plants get last plantId
            var isInterval = ''+itm.isInterval;
            var useWms = ''+itm.useWms;
            var grInterval = parseInt(itm.grInterval);
            ApiService.fetchCcaPlantsLast()
            .then(res => {
              res.data.map(val => {
                // check and insert cca_plant_realtime
                ApiService.deleteCcaPlantRealtimes(val.plantId);
                ApiService.addCcaPlantRealtimes({
                  "plantId": val.plantId,
                  "isInterval": isInterval,
                  "grInterval": grInterval
                });

                // check and insert cca_plant_wms
                ApiService.deleteCcaPlantWms(val.plantId);
                ApiService.addCCcaPlantWms({
                  "plantId": val.plantId,
                  "useWms": useWms
                });
              });

            }); 
            //console.log(JSON.stringify(itm));
            //console.log(JSON.stringify(itm.plantId));
            alertMessage({message: 'Data '+itm.plantDescr+' added successfully.'})
          }}
          onCancel={itm =>{
            alert("Data reset");
          }}
        />
        
            <div className="col-xs-3 col-md-5">
              <TextInput
              placeHolder="Plant Name"
                  helpText= "Plant Name"
                  value={plantDescr}
                />
            </div>
            <div className="col-xs-3 col-md-5">
              <TextInput
                placeHolder="Plant Code"
                helpText= "Plant Code"
                value={plantCode}
              />
            </div>
            <div className="col-sm-4">
              <CheckBox
                placeHolder="GR does not use real-time data"
                value={isInterval}
              />
            </div>
            <div className="col-xs-6 col-md-2">
              <TextInput
                placeHolder="Second(s)"
                helpText= "Number"
                value={grInterval}
              />
            </div>
            <div className="col-sm-8">
              <CheckBox
                placeHolder="Using EWM System"
                value={useWms}
              />
            </div>
            <div className="col-sm-12">
              <div className="form-actions noborder">
                <input type="submit" 
                  value="Update" 
                  className="btn blue"/>
              </div>
            </div>
          
        </div>

    }
  }

  /* fromBodyRender(options) {
    if(this.state.data !== null){
      const {item, binding} = options
      
    }
  } */
}

export default EditForm
