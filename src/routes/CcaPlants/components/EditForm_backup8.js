import React, { Component } from 'react';
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

//const TABS_DATA = []
var dateStr = "";
var date = new Date();
let plantDescr, plantCode, grInterval, isInterval, useWms, recCreated, recDatetime;
var items = [];

class EditForm extends BaseContainer {
  /* state={
      dataState: null,
      isLoading: true
    } */
  constructor(props) {
    super(props);
    this.fromBodyRender = this.fromBodyRender.bind(this);
    this.state = { 
      data: null,
      loading: true
    };
  }


  async componentDidMount() {
  //componentWillMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    //alert(dateStr)

    var url = window.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    var key_id = idVal;
    console.log(key_id);
     try {
        //Assign the promise unresolved first then get the data using the json method. 
        ApiService.fetchGetCcaPlantbyId(key_id)
        .then(response => {
          this.setState({ 
            data: response.data,
            loading: false
          }) 
          /*  response.data.map(data => {
            items.push({
              plantDescr:data.plantDescr,
              plantCode:data.plantCode,
              grInterval:data.grInterval,
              isInterval:data.isInterval,
              useWms:data.useWms,
              recCreated: dateStr,
              recDatetime: dateStr
            }) 
            plantDescr=data.plantDescr;
            plantCode=data.plantCode;
            grInterval=data.grInterval;
            isInterval=data.isInterval;
            useWms=data.useWms;
            recCreated= dateStr;
            recDatetime= dateStr; 
          }); */
        })
    } catch(err) {
        console.log("Error fetching data-----------", err);
    } 
    

    /* try {
        //Assign the promise unresolved first then get the data using the json method. 
        await ApiService.fetchGetCcaPlantbyId(key_id)
        .then(response => {
          this.setState({ 
            dataState: response.data,
            loading: false
          })
        })
    } catch(err) {
        console.log("Error fetching data-----------", err);
    } */
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    //let data = this.state.dataState;
    //const { isLoading, isLoaded, data } = this.props;
    //if (isLoading) return <Loader />;
    
    if(this.state.data !== null){
      //{items.map(data => {
        /* plantDescr=`${data.plantDescr}`;
        plantCode=`${data.plantCode}`;
        grInterval=`${data.grInterval}`;
        isInterval=`${data.isInterval}`;
        useWms=`${data.useWms}`;
        recCreated: dateStr;
        recDatetime: dateStr; */
      //})}
    //}
      {this.state.data.map(item => {
        //console.log(JSON.stringify(data.plantDescr));
        //console.log(JSON.stringify(item.plantDescr));
        //console.log(`${item.plantDescr}`);
        //console.log(item);
        //plantDescr, plantCode, grInterval, isInterval, useWms, recCreated, recDatetime;
        plantDescr=`${item.plantDescr}`;
        plantCode=`${item.plantCode}`;
        grInterval=`${item.grInterval}`;
        isInterval=`${item.isInterval}`;
        useWms=`${item.useWms}`;
        recCreated=dateStr;
        recDatetime=dateStr;
      })}
      /* plantDescr="plantDescr";
      plantCode="plantCode";
      grInterval="items.grInterval";
      isInterval="isInterval";
      useWms="useWms";
      recCreated: dateStr;
      recDatetime: dateStr; */
      //console.log(items);
      //console.log({items});
      //console.log(JSON.stringify(items));
      //console.log(JSON.stringify(items));
      //console.log({items});
      //console.log(items.plantDescr);
    
      
        return <div className="row">
          <div className="col-xs-3 col-md-5">
            <TextInput
            placeHolder="Plant Name"
                helpText= "Plant Name"
                value={plantDescr}
              />
          </div>
          <div className="col-xs-3 col-md-5">
            <TextInput
              placeHolder="Plant Code"
              helpText= "Plant Code"
              value={plantCode}
            />
          </div>
          <div className="col-sm-4">
            <CheckBox
              placeHolder="GR does not use real-time data"
              value={isInterval}
            />
          </div>
          <div className="col-xs-6 col-md-2">
            <TextInput
              placeHolder="Second(s)"
              helpText= "Number"
              value={grInterval}
            />
          </div>
          <div className="col-sm-8">
            <CheckBox
              placeHolder="Using EWM System"
              value={useWms}
            />
          </div>
        </div>

    }
  }

  fromBodyRender(options) {
    if(this.state.data !== null){
      const {item, binding} = options
      
    }
  }
}

export default EditForm
