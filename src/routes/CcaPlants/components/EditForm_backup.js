import React, { Component } from 'react';
import ApiService from '../../../service/ApiService'
import {alertMessage} from '../../../utils'
import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

var dateStr = "";
var date = new Date();
var key_id = 0;

export default class EditForm extends Component {
  constructor(props) {
    super(props);
    this.onChangePlantDescr = this.onChangePlantDescr.bind(this);
    this.onChangePlantCode = this.onChangePlantCode.bind(this);
    this.onChangeRecCreated = this.onChangeRecCreated.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      plantDescr: '',
      plantCode: '',
      recCreated:''
    }
  }

  componentDidMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);

    var url = window.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    key_id = idVal;

    ApiService.fetchCcaPlantsById(key_id)
      .then(response => {
        this.setState({ 
          plantId: response.data.plantId,
          plantDescr: response.data.plantDescr, 
          plantCode: response.data.plantCode
        });
      })
      .catch(function (error) {
        console.log(error);
      })
    }

  onChangePlantDescr(e) {
    this.setState({
      plantDescr: e.target.value
    });
  }
  onChangePlantCode(e) {
    this.setState({
      plantCode: e.target.value
    })  
  }
  onChangeRecCreated(e) {
    this.setState({
      recCreated: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();
    var paramsId = parseInt(key_id, 10);  // 10 Decimal
    const obj = {
      plantId: paramsId,//this.props.match.params.id,
      plantDescr: this.state.plantDescr,
      plantCode: this.state.plantCode,
      recCreated: dateStr,
      recDatetime: dateStr
    };
    console.log(obj);
    ApiService.editCcaPlants(paramsId,obj)
    .then(res => {
      //this.props.history.push(View)
      this.props.history.push("/View")
    })
    alertMessage({message: 'Data added successfully.'})
  }
 
  render() {
    return (
        <div className="portlet light bordered">
          <div className="portlet-title">
            <Link to="/View" className="btn sbold blue" id="btnView">VIEW DATA<i className="fa fa-list-alt "></i></Link>
            <br />
            <div className="caption font-red-sunglo">
              <i className="icon-settings font-red-sunglo"></i><span className="caption-subject bold uppercase">Edit Plants</span>
            </div>
          </div>
          <div className="portlet-body form">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <div className="row">
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Plant Name :  </label>
                      <input 
                        type="text" 
                        className="form-control" 
                        value={this.state.plantDescr}
                        onChange={this.onChangePlantDescr}
                        />
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Plant Code : </label>
                      <input type="text" 
                        className="form-control"
                        value={this.state.plantCode}
                        onChange={this.onChangePlantCode}
                        />
                    </div>
                  </div>
                  <div className="col-sm-12">
                    <div className="md-checkbox">
                      <input type="checkbox" className="form-check-input" id="chkGr" checked />
                      <label for="chkGr"><span className="inc"></span><span className="check"></span><span className="box"></span> GR does not use real-time data</label>
                    </div>
                  </div>
                  <div className="col-sm-12">
                    <div className="md-checkbox">
                      <input type="checkbox" className="form-check-input" id="chkEwm" checked />
                      <label for="chkEwm"><span className="inc"></span><span className="check"></span><span className="box"></span> Using EWM System</label>
                    </div>
                  </div>
                  <div className="col-sm-12">
                    <div className="form-actions noborder">
                      <input type="submit" 
                        value="Update" 
                        className="btn blue"/>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
    )
  }
}