import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

const TABS_DATA = []
var dateStr = "";
var date = new Date();
var plant_id = "";

class AddForm extends BaseContainer {
  constructor(props) {
    super(props);
    this.fromBodyRender = this.fromBodyRender.bind(this);
    this.fromSubmit = this.fromSubmit.bind(this);
    this.state = {
      isLoading: true
    };
  }


  componentWillMount() {
    this.setState({
      title: "Plant Area"
    })
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    //alert(dateStr)
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    const item = {
      plantDescr: "",
      plantCode: "",
      grInterval: 0,
      isInterval: 0,
      useEwm: 0,
      recCreated: dateStr,
      recDatetime: dateStr
    }

    return <DataForm
        title="Add Plants"
        hasCancel={true}
        item={item}
        bodyRender={this.fromBodyRender}
        onSubmit={itm => {
          //var stsIns = false;
          // insert cca_plants
          ApiService.addCcaPlants(itm)
          .then(res => {
            if(!res) throw new Error(res.status);
            else 
              this.fromSubmit(itm);
            //console.log(JSON.stringify(itm));
            //console.log(JSON.stringify(itm.plantId));
            alertMessage({message: 'Data '+itm.plantDescr+' added successfully.'})
          })
          .catch((error) => {
            console.log('error: ' + error);
            ApiService.fetchCcaPlantsLast()
            .then(res => {
              res.data.map(val => {
                ApiService.deleteCcaPlants(val.plantId)
              })
            });
            alertMessage({message: 'Data '+itm.plantDescr+' added successfully.'})
          });
        }}
        onCancel={itm =>{
          alert("Data reset");
        }}
      />
  }

  fromSubmit(itm) {
    // select cca_plants get last plantId
    var isInterval = ''+itm.isInterval;
    //var useWms = ''+itm.useWms;
    var useEwm = ''+itm.useEwm;
    var grInterval = parseInt(itm.grInterval);
    ApiService.fetchCcaPlantsLast()
    .then(res => {
      res.data.map(val => {
        // check and insert cca_plant_realtime
        //ApiService.deleteCcaPlantRealtimes(val.plantId);
        ApiService.addCcaPlantRealtimes({
          "plantId": val.plantId,
          "isInterval": isInterval,
          "grInterval": grInterval
        });

        // check and insert cca_plant_wms
        //ApiService.deleteCcaPlantWms(val.plantId);
        ApiService.addCCcaPlantWms({
          "plantId": val.plantId,
          "useWms": useEwm
        });

        // check and insert cca_plant_ewm
        //ApiService.deleteCcaPlantEwms(val.plantId);
        ApiService.addCcaPlantEwms({
          "plantId": val.plantId,
          "useEwm": useEwm
        });
      });
    })
  }

  fromBodyRender(options) {
    const {item, binding} = options
    return <div className="row">
      <div className="col-6 col-sm-9">
        <TextInput
          placeHolder="Plant Name"
          helpText= "Plant Name"
          {...binding("plantDescr")}
          />
      </div>
      <div className="col-6 col-sm-9">
        <TextInput
          placeHolder="Plant Code"
          helpText= "Plant Code"
          {...binding("plantCode")}
        />
      </div>
      <div className="col-sm-4">
        <CheckBox
          placeHolder="GR does not use real-time data"
          {...binding("isInterval")}
        />
      </div>
      <div className="col-xs-6 col-md-2">
        <TextInput
          placeHolder="Second(s)"
          helpText= "Number"
          {...binding("grInterval")}
        />
      </div>
      <div className="col-sm-8">
        <CheckBox
          placeHolder="Using EWM System"
          {...binding("useEwm")}
        />
      </div>
    </div>
  }
}

export default AddForm
