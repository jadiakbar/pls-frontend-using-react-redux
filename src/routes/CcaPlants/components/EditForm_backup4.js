import React, { Component } from 'react';
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

//const TABS_DATA = []
var dateStr = "";
var date = new Date();

class EditForm extends BaseContainer {
  state={
      /* plantId: dataState.plantId,
      plantDescr: dataState.plantDescr,
      plantCode: dataState.plantCode,
      grInterval: dataState.grInterval, */
      /*dataState: [{
          plantId: "plantId"
        }, {
          plantDescr: "plantDescr"
        }, {
          plantCode: "plantCode"
        }, {
          grInterval: "grInterval"
        }],*/
      dataState: '',
      isLoading: true
    } 


  componentDidMount() {
  //componentWillMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    //alert(dateStr)

    var url = window.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    var key_id = idVal;
    console.log(key_id);

    ApiService.fetchGetCcaPlantbyId(key_id)
      .then(response => {
        /* this.setState({ 
          plantId: response.data.plantId,
          plantDescr: response.data.plantDescr, 
          plantCode: response.data.plantCode,
          isInterval: response.data.isInterval,
          grInterval: response.data.grInterval,
          isLoading: false
        }); */
        /* this.setState({ 
          dataState: response.data,
          isLoading: false
        });*/
        this.setState(nextState => ({ 
          dataState: response.data
        }));
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    //console.log(this.state.dataState.plantDescr);
    //console.log(JSON.stringify(this.state.dataState.plantDescr));
      //let { dataState } = this.state;
      /* if(this.state.dataState){
        {this.state.dataState.map(data => {
          //return `${data.lat},${data.lon}|`;
          console.log(`${data.plantDescr}`);
        })}
      } */
      

      /*
      const reducedArray = dataState.reduce((acc, curr) => `${acc}${curr.plantDescr},${curr.plantCode}|` ,'');
      console.log(reducedArray);
      */
    
      //const plantDescrVal = this.state.dataState.plantDescr;
      //const plantCodeVal = this.state.dataState.plantCode;
      //console.log('ini item state:'+JSON.stringify(this.state.dataState));
      //console.log('ini item plantDescr:'+JSON.stringify(this.state.plantDescr));
      //console.log(this.state.dataState);
      //console.log(this.state.dataState.plantDescr);
      //console.log(JSON.stringify(this.state.dataState));
      //console.log('ini plantDescr:'+plantDescrVal);
     // return this.state.dataState;

     const valState = this.state.dataState;
     let item = {}

    if(this.state.dataState){
      {this.state.dataState.map(data => {
        const item = {
          plantDescr: `${data.plantDescr}`,
          plantCode: `${data.plantCode}`,
          grInterval: `${data.grInterval}`,
          isInterval: 1,
          useWms: 0,
          recCreated: dateStr,
          recDatetime: dateStr
        }; 
        console.log(item);
        //console.log('ini item plantDescr:'+JSON.stringify(this.state.dataState.plantDescr));
        //console.log('ini item plantCode:'+JSON.stringify(this.state.dataState.plantCode));
    })}

        return <DataForm
            title="Edit Plants"
            hasCancel={true}
            item={item}
            bodyRender={this.fromBodyRender}
            onSubmit={itm => {
              // insert cca_plants
              ApiService.addCcaPlants(itm)
              .then(res => {
                //this.props.history.push(View)
              });

              // select cca_plants get last plantId
              var isInterval = ''+itm.isInterval;
              var useWms = ''+itm.useWms;
              var grInterval = parseInt(itm.grInterval);
              ApiService.fetchCcaPlantsLast()
              .then(res => {
                res.data.map(val => {
                  // check and insert cca_plant_realtime
                  ApiService.deleteCcaPlantRealtimes(val.plantId);
                  ApiService.addCcaPlantRealtimes({
                    "plantId": val.plantId,
                    "isInterval": isInterval,
                    "grInterval": grInterval
                  });

                  // check and insert cca_plant_wms
                  ApiService.deleteCcaPlantWms(val.plantId);
                  ApiService.addCCcaPlantWms({
                    "plantId": val.plantId,
                    "useWms": useWms
                  });
                });

              }); 
              //console.log(JSON.stringify(itm));
              //console.log(JSON.stringify(itm.plantId));
              alertMessage({message: 'Data '+itm.plantDescr+' added successfully.'})
            }}
            onCancel={itm =>{
              alert("Data reset");
            }}
          />
     
    }
  }

  fromBodyRender(options) {
    const {item, binding} = options
    return <div className="row">
      <div className="col-xs-3 col-md-5">
        <TextInput
        placeHolder="Plant Name"
            helpText= "Plant Name"
            {...binding("plantDescr")}
            value={item.plantDescr}
          />
      </div>
      <div className="col-xs-3 col-md-5">
        <TextInput
          placeHolder="Plant Code"
          helpText= "Plant Code"
          {...binding("plantCode")}
        />
      </div>
      <div className="col-sm-4">
        <CheckBox
          placeHolder="GR does not use real-time data"
          {...binding("isInterval")}
        />
      </div>
      <div className="col-xs-6 col-md-2">
        <TextInput
          placeHolder="Second(s)"
          helpText= "Number"
          {...binding("grInterval")}
        />
      </div>
      <div className="col-sm-8">
        <CheckBox
          placeHolder="Using EWM System"
          {...binding("useWms")}
        />
      </div>
    </div>
  }
}

export default EditForm
