import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import Table from '../../../components/Table'
import ApiService from '../../../service/ApiService'
import {alertMessage} from '../../../utils'
import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';
import Edit from '../components/EditForm';


//const TABLE_DATA = []
var data = [];

class TableView extends BaseContainer {
  state={
    header: [{
      title:"#",
      prop: "no"
    }, {
      title:"Plant Area",
      prop: "plantDescr"
    }, {
      title:"Plant Code",
      prop:"plantCode"
    }, {
      title:"Action"
    }],
    data: data
  }

  /* handleClick(value) {
    alert('Click happened' + i);
  } */

  onClick(i) {
    alert('Click happened' + i);
  }

  componentWillMount() {
    this.setState({
      title: "Plant Area"
    })
    ApiService.fetchCcaPlants()
      .then(response => {
        this.setState({ 
          data: response.data 
        });
      })
      this.showLoading(true)
        setTimeout(() => {
          this.showLoading(false)
        }, 500)

    if(data == null || data == undefined) {
      this.showLoading(true)
    } else {
      setTimeout(() => {
        this.showLoading(false)
      }, 500)
    }
  }



  _renderContent() {
    console.log(this.state)
    return <div className="portlet body">
      <Table
        searchableProperties={["plantDescr","plantCode"]}
        hasTopBar={true}
        headers={this.state.header}
        data={this.state.data}
        rowRender={this._renderRow}
        onAddNewHandler={this.onAddNewClick.bind(this)}
      />
    </div>
  }

  onAddNewClick() {
    alert('OK')
  }
  
  _renderRow(itm, i) {
    return <tr key={i} className="odd gradeX">
      <td>{i+1}</td>
      <td>{itm.plantDescr}</td>
      <td>{itm.plantCode}</td>
      <td>
        <div className="btn-group">
          <button className="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
            <i className="fa fa-angle-down"></i>
          </button>
          <ul className="dropdown-menu pull-left" role="menu">
            <li>
              <a href="javascript:;">
                <i className="icon-flag"></i> Area Name
                <span className="badge badge-success">4</span>
              </a>
            </li>
            <li>
              <a href="javascript:;">
                <i className="glyphicon glyphicon-tags"></i> Area Code </a>
            </li>
            <li className="divider"> </li>
            <li>
              <Link to={'/Edit/Find?id='+itm.plantId } id="btnEdit"><i className="glyphicon glyphicon-pencil"></i> Edit </Link>
            </li>
            <li>
              <a onClick={e => handleClick(itm.plantId)} >
                <i className="glyphicon glyphicon-trash"></i> Delete </a>
            </li>
          </ul>
        </div>
      </td>
    </tr>
  }
}

function handleClick(val) {
  //alert(e)
  var result = confirm("Are you sure...?");
  if (result) {
    ApiService.deleteCcaPlants(val);
    ApiService.deleteCcaPlantRealtimes(val);
    ApiService.deleteCcaPlantWms(val)
    ApiService.deleteCcaPlantEwms(val)
    .then(response => {
      this.props.history.push("/View")
      alertMessage({message: 'Data '+ response.data.plantDescr +' deleted successfully'})
    });
  }
}

export default TableView
