import React, { Component } from 'react';
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

var dateStr = "";
var date = new Date();
//let plantDescr, plantCode, grInterval, isInterval, useWms, recCreated, recDatetime;
var key_id = "";


class EditForm extends BaseContainer {
  constructor(props) {
    super(props);
    this.onChangePlantDescr = this.onChangePlantDescr.bind(this);
    this.onChangePlantCode = this.onChangePlantCode.bind(this);
    this.onChangeIsInterval = this.onChangeIsInterval.bind(this);
    this.onChangeGrInterval = this.onChangeGrInterval.bind(this);
    this.onChangeUseWms = this.onChangeUseWms.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.state = { 
      //data: null,
      plantId: '',
      plantDescr: '',
      plantCode: '',
      isInterval:'',
      grInterval: '',
      useWms:'',
      loading: true
    };
  }


  async componentDidMount() {
    //componentWillMount() {
    this.setState({
      title: "Plant Area"
    })
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    //alert(dateStr)

    var url = window.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    key_id = idVal;
    console.log(key_id);
     try {
        //Assign the promise unresolved first then get the data using the json method. 
        ApiService.fetchGetCcaPlantbyId(key_id)
        .then(response => {
          this.setState({ 
            //data: response.data,
            plantId: response.data.plantId,
            plantDescr: response.data.plantDescr, 
            plantCode: response.data.plantCode,
            isInterval: response.data.isInterval,
            grInterval: response.data.grInterval, 
            useWms: response.data.useWms,
            loading: false
          }) 
        })
    } catch(err) {
        console.log("Error fetching data-----------", err);
    } 
  }

  onChangePlantDescr(e) {
    this.setState({
      plantDescr: e.target.value
    });
    alert('berhasil');
  }
  onChangePlantCode(e) {
    this.setState({
      plantCode: e.target.value
    })  
  }

  onChangeGrInterval(e) {
    this.setState({
      grInterval: e.target.value
    })  
  }

  onChangeIsInterval(e) {
    this.setState({
      isInterval: e.target.value
    })  
  }

  onChangeUseWms(e) {
    this.setState({
      useWms: e.target.value
    })  
  }

  onSubmit(e) {
    e.preventDefault()
    /* e.preventDefault();
    var paramsId = parseInt(key_id, 10);  // 10 Decimal
    const obj = {
      plantId: paramsId,//this.props.match.params.id,
      plantDescr: this.state.plantDescr,
      plantCode: this.state.plantCode,
      recCreated: dateStr,
      recDatetime: dateStr
    };
    console.log(obj);
    ApiService.editCcaPlants(paramsId,obj)
    .then(res => {
      //this.props.history.push(View)
      this.props.history.push("/View")
    })
    alertMessage({message: 'Data edit successfully.'}) */
    /*  let obj = [];
    {this.state.data.map(item => {
      obj = {
        plantDescr: `${item.plantDescr}`,
        plantCode: `${item.plantCode}`,
        grInterval: `${item.grInterval}`,
        isInterval: `${item.isInterval}`,
        useWms: `${item.useWms}`,
        recCreated: dateStr,
        recDatetime: dateStr
      }
    })} */
    var paramsId = parseInt(key_id, 10);  // 10 Decimal
    const obj = {
      plantId: paramsId,//this.props.match.params.id,
      plantDescr: this.state.plantDescr,
      plantCode: this.state.plantCode,
      grInterval: this.state.grInterval,
      isInterval: this.state.isInterval,
      useWms: this.state.useWms,
      recCreated: dateStr,
      recDatetime: dateStr
    };
    ;
    console.log(obj);
    alertMessage({message: 'Data edit successfully.'})

    /*
    // select cca_plants get last plantId
    var isInterval = ''+itm.isInterval;
    var useWms = ''+itm.useWms;
    var grInterval = parseInt(itm.grInterval);
    ApiService.fetchCcaPlantsLast()
      .then(res => {
        res.data.map(val => {
          // check and insert cca_plant_realtime
          ApiService.deleteCcaPlantRealtimes(val.plantId);
          ApiService.addCcaPlantRealtimes({
            "plantId": val.plantId,
            "isInterval": isInterval,
            "grInterval": grInterval
          });

          // check and insert cca_plant_wms
          ApiService.deleteCcaPlantWms(val.plantId);
          ApiService.addCCcaPlantWms({
            "plantId": val.plantId,
            "useWms": useWms
          });
        });

      }); 
      //console.log(JSON.stringify(itm));
      //console.log(JSON.stringify(itm.plantId));
      alertMessage({message: 'Data '+itm.plantDescr+' added successfully.'}) */
  }

  onCancel(e) {

  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    //if(this.state.data !== null){
      /* {this.state.data.map(item => {
        plantDescr=`${item.plantDescr}`;
        plantCode=`${item.plantCode}`;
        grInterval=`${item.grInterval}`;
        isInterval=`${item.isInterval}`;
        useWms=`${item.useWms}`;
        recCreated=dateStr;
        recDatetime=dateStr;
      })} */
      console.log(this.state.plantDescr);
      
        return <div className="portlet light bordered">
            <div className="portlet-title">
              <Link to="/View" className="btn sbold blue" id="btnView">VIEW DATA<i className="fa fa-list-alt "></i></Link>
              <br />
              <div className="caption font-red-sunglo">
                <i className="icon-settings font-red-sunglo"></i><span className="caption-subject bold uppercase">Edit Plants</span>
              </div>
            </div>
            <p></p>
            <div className="row">
              <form onSubmit={this.onSubmit}>
                <div className="col-xs-3 col-md-5">
                  <TextInput
                  placeHolder="Plant Name"
                      helpText= "Plant Name"
                      value={this.state.plantDescr}
                      onChange={this.onChangePlantDescr}
                    />
                </div>
                <div className="col-xs-3 col-md-5">
                  <TextInput
                    placeHolder="Plant Code"
                    helpText= "Plant Code"
                    value={this.state.plantCode}
                    onChange={this.onChangePlantDescr}
                  />
                </div>
                <div className="col-sm-4">
                  <CheckBox
                    placeHolder="GR does not use real-time data"
                    value={this.state.isInterval}
                    onChange={this.onChangeIsInterval}
                  />
                </div>
                <div className="col-xs-6 col-md-2">
                  <TextInput
                    placeHolder="Second(s)"
                    helpText= "Number"
                    value={this.state.grInterval}
                    onChange={this.onChangeGrInterval}
                  />
                </div>
                <div className="col-sm-8">
                  <CheckBox
                    placeHolder="Using EWM System"
                    value={this.state.useWms}
                    onChange={this.onChangeUseWms}
                  />
                </div>
                <div className="col-sm-12">
                  <div className="form-actions noborder">
                    <p></p>
                      <input type="submit" value="Update" className="btn blue"/>
                  </div>
                </div>
              </form>
            </div>
          </div>
    //}
  }

  /* fromBodyRender(options) {
    if(this.state.data !== null){
      const {item, binding} = options
      
    }
  } */
}

export default EditForm
