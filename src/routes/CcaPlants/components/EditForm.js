import React, { Component } from 'react';
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

var dateStr = "";
var date = new Date();
var key_id = "";

class EditForm extends BaseContainer {
  constructor(props) {
    super(props);
    this.fromBodyRender = this.fromBodyRender.bind(this);
    this.fromSubmit = this.fromSubmit.bind(this);
    this.state = { 
      dataState: null,
      plantId: '',
      plantDescr: '',
      plantCode: '',
      isInterval:'',
      grInterval: '',
      useWms:'',
      useEwm:'',
      recCreated: '',
      recDatetime: '',
      isLoading: true
    };
  }


  async componentDidMount() {
  //componentWillMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    //alert(dateStr)

    var url = window.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    key_id = idVal;
    console.log(key_id);

    try {
        //Assign the promise unresolved first then get the data using the json method. 
        await ApiService.fetchGetCcaPlantbyId(key_id)
        .then(response => {
          this.setState({ 
            dataState: response.data
          })
        })
        //console.log(this.state.dataState);
        if(this.state.dataState !== null){
          {this.state.dataState.map(item => {
            this.setState({ 
              plantId: `${item.plantId}`,
              plantDescr: `${item.plantDescr}`,
              plantCode: `${item.plantCode}`,
              isInterval: `${item.isInterval}`,
              grInterval: `${item.grInterval}`,
              useWms: `${item.useWms}`,
              useEwm: `${item.useEwm}`,
              recCreated: dateStr,
              recDatetime: dateStr
            })
          })}
          
        }
    } catch(err) {
        console.log("Error fetching data-----------", err);
    }
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    if(this.state.plantDescr) {
      var paramsId = parseInt(key_id, 10);  // 10 Decimal
      let item = { 
        plantId: paramsId,
        plantDescr: this.state.plantDescr, 
        plantCode: this.state.plantCode,
        grInterval: this.state.grInterval,
        isInterval: this.state.isInterval, 
        useWms: this.state.useWms,
        useEwm: this.state.useEwm,
        recCreated: this.state.recCreated, 
        recDatetime: this.state.recDatetime, 
      };
        console.log(this.state.plantDescr);

          return <DataForm
            title="Edit Plants"
            hasCancel={true}
            item={item}
            bodyRender={this.fromBodyRender}
            onSubmit={itm => {
              // insert cca_plants
              ApiService.editCcaPlants(itm.plantId,itm)
              .then(res => {
                if(!res) throw new Error(res.status);
                else 
                  this.fromSubmit(itm);
                  this.props.history.push("/View");
                  alertMessage({message: 'Data '+itm.plantDescr+' update successfully.'})
                });
              //console.log(JSON.stringify(itm));
              //console.log(JSON.stringify(itm.plantId));
            }}
            onCancel={itm =>{
              alert("Data reset");
            }}
          />
    }
  }

  fromSubmit(itm) {
    // select cca_plants get last plantId
    var isInterval = ''+itm.isInterval;
    var useWms = ''+itm.useWms;
    var useEwm = ''+itm.useEwm;
    var grInterval = parseInt(itm.grInterval);
    ApiService.fetchCcaPlantsLast()
    .then(res => {
      res.data.map(val => {
        // check and insert cca_plant_realtime
        ApiService.deleteCcaPlantRealtimes(val.plantId);
        ApiService.addCcaPlantRealtimes({
          "plantId": val.plantId,
          "isInterval": isInterval,
          "grInterval": grInterval
        });

        // check and insert cca_plant_wms
        ApiService.deleteCcaPlantWms(val.plantId);
        ApiService.addCCcaPlantWms({
          "plantId": val.plantId,
          "useWms": useWms
        }); 

        // check and insert cca_plant_ewm
        ApiService.deleteCcaPlantEwms(val.plantId);
        ApiService.addCcaPlantEwms({
          "plantId": val.plantId,
          "useEwm": useEwm
        });
      });

    }); 
  }

  fromBodyRender(options) {
    const {item, binding} = options
    return <div className="row">
      <div className="col-xs-3 col-md-5">
        <TextInput
        placeHolder="Plant Name"
            helpText= "Plant Name"
            {...binding("plantDescr")}
          />
      </div>
      <div className="col-xs-3 col-md-5">
        <TextInput
          placeHolder="Plant Code"
          helpText= "Plant Code"
          {...binding("plantCode")}
        />
      </div>
      <div className="col-sm-4">
        <CheckBox
          placeHolder="GR does not use real-time data"
          {...binding("isInterval")}
        />
      </div>
      <div className="col-xs-6 col-md-2">
        <TextInput
          placeHolder="Second(s)"
          helpText= "Number"
          {...binding("grInterval")}
        />
      </div>
      <div className="col-sm-8">
        <CheckBox
          placeHolder="Using EWM System"
          {...binding("useEwm")}
        />
      </div>
    </div>
  }
}

export default EditForm
