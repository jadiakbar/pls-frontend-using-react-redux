import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import Table from '../../../components/Table'
import ApiService from '../../../service/ApiService'
import {alertMessage} from '../../../utils'
import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';
import Edit from '../components/EditForm';


//const TABLE_DATA = []
var data = [];

class TableView extends BaseContainer {
  state={
    header: [{
      title:"#",
      prop: "no"
    }, {
      title:"Device Name",
      prop: "deviceName"
    }, {
      title:"Device Model",
      prop: "deviceModel"
    }, {
      title:"Device GUID",
      prop: "deviceGuid"
    }, {
      title:"Device IMEI",
      prop: "deviceImei"
    }, {
      title:"Device Serial",
      prop: "deviceSerial"
    }, {
        title:"Action"
      }],
      data: data
    }

  componentWillMount() {
    this.setState({
      title: "Devices"
    })
    ApiService.fetchCcaDevices()
      .then(response => {
        this.setState({ 
          data: response.data 
        });
      })
      this.showLoading(true)
        setTimeout(() => {
          this.showLoading(false)
        }, 500);
  }


  _renderContent() {
    console.log(this.state)
    return <div className="portlet body">
      <Table
        searchableProperties={["scannerName", "scannerIp","scannerPort"]}
        hasTopBar={true}
        headers={this.state.header}
        data={this.state.data}
        rowRender={this._renderRow}
        onAddNewHandler={this.onAddNewClick.bind(this)}
      />
    </div>
  }

  onAddNewClick() {
    
  }

  _renderRow(itm, i) {
    return <tr key={i} className="odd gradeX">
      <td>{i+1}</td>
      <td>{itm.deviceName}</td>
      <td>{itm.deviceModel}</td>
      <td>{itm.deviceGuid}</td>
      <td>{itm.deviceImei}</td>
      <td>{itm.deviceSerial}</td>
      <td>
        <div className="btn-group">
          <button className="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
            <i className="fa fa-angle-down"></i>
          </button>
          <ul className="dropdown-menu pull-left" role="menu">
            <li>
              <a href="javascript:;">
                <i className="icon-flag"></i> Device Name
                <span className="badge badge-success">4</span>
              </a>
            </li>
            <li>
              <a href="javascript:;">
                <i className="glyphicon glyphicon-tags"></i> Area Code </a>
            </li>
            <li className="divider"> </li>
            <li>
              <a href="javascript:;">
                <i className="glyphicon glyphicon-trash"></i> Delete </a>
            </li>
          </ul>
        </div>
      </td>
    </tr>
  }
}

export default TableView
