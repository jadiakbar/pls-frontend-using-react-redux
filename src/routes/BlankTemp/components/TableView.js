import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import Table from '../../../components/Table'
import ApiService from '../../../service/ApiService'
//import {Button} from '../../../components/UI'
import {alertMessage} from '../../../utils'
//import axios from 'axios'
//import cors from 'cors'
// Add the component for AddForm 08 Feb 2020
//import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';
import Edit from '../components/EditForm';



//const TABLE_DATA = []
var data = [];

class TableView extends BaseContainer {
  state={
    header: [{
      title:"#",
      prop: "no"
    }, {
      title:"Plant Area",
      prop: "plantDescr"
    }, {
      title:"Plant Code",
      prop:"plantCode"
    }, {
      title:"Action"
    }],
    data: data
  }

  componentWillMount() {
    /*
    this.setState({
      title: "Data Plant Area"
    })
    
    const apiUrl = './api/CcaPlants';
      fetch(apiUrl)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              data: result
              //TABLE_DATA: result
            });
          },
          (error) => {
            this.setState({ error });
          }
        )
      this.showLoading(true)
        setTimeout(() => {
          this.showLoading(false)
        }, 500);
        */

    ApiService.fetchCcaPlants()
      .then(response => {
        this.setState({ 
          data: response.data 
        });
      })
      this.showLoading(true)
        setTimeout(() => {
          this.showLoading(false)
        }, 500);
  }



  _renderContent() {
    console.log(this.state)
    return <Table
          searchableProperties={["plantDescr","plantCode"]}
          hasTopBar={true}
          headers={this.state.header}
          data={this.state.data}
          rowRender={this._renderRow}
          onAddNewHandler={this.onAddNewClick.bind(this)}
        />
  }

  /*
  hiddenContent(e) {
    //document.getElementById("divTable").style.display = "none";
    document.getElementById("btnAdd").style.display = 'none';
    document.getElementById("divTable").innerHTML = '<div></div>';
  }
  */


  onAddNewClick() {
    this.setState({
      title: "Add Plant Area"
    })
  }

  
  _renderRow(itm, i) {
    return <tr key={i} className="odd gradeX">
      <td>{i+1}</td>
      <td>{itm.plantDescr}</td>
      <td>{itm.plantCode}</td>
      <td>
        <div className="btn-group">
          <button className="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
            <i className="fa fa-angle-down"></i>
          </button>
          <ul className="dropdown-menu pull-left" role="menu">
            <li>
              <a href="javascript:;">
                <i className="icon-flag"></i> Area Name
                <span className="badge badge-success">4</span>
              </a>
            </li>
            <li>
              <a href="javascript:;">
                <i className="glyphicon glyphicon-tags"></i> Area Code </a>
            </li>
            <li className="divider"> </li>
            <li>
              <a href="javascript:;">
                <i className="glyphicon glyphicon-trash"></i> Delete </a>
            </li>
          </ul>
        </div>
      </td>
    </tr>
  }
}

export default TableView
