import React, { Component } from 'react';
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

var dateStr = "";
var date = new Date();
var key_id = "";

class EditForm extends BaseContainer {
  constructor(props) {
    super(props);
    this.fromBodyRender = this.fromBodyRender.bind(this);
    this.fromSubmit = this.fromSubmit.bind(this);
    this.state = { 
      dataState: null,
      printerId: '',
      printerName: '',
      printerDescr: '',
      printerIp:'',
      printerPort: '',
      isOnline:'',
      recCreated:'',
      recDatetime: ''
    };
  }


  async componentDidMount() {
  //componentWillMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    //alert(dateStr)

    var url = window.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    key_id = idVal;
    console.log(key_id);

    try {
        //Assign the promise unresolved first then get the data using the json method. 
        await ApiService.fetchCcaPrintersById(key_id)
        .then(response => {
          this.setState({ 
            dataState: response.data
          })
        })
        console.log('ini data si state=>'+JSON.stringify(this.state.dataState));
        if(this.state.dataState !== null){
          {this.state.dataState.map(item => {
            this.setState({ 
              printerId: `${item.printerId}`,
              printerName: `${item.printerName}`,
              printerDescr: `${item.printerDescr}`,
              printerIp: `${item.printerIp}`,
              printerPort: `${item.printerPort}`,
              isOnline: `${item.isOnline}`,
              recCreated: dateStr,
              recDatetime: dateStr
            })
          })}
        }
    } catch(err) {
        console.log("Error fetching data-----------", err);
    }
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    if(this.state.printerName) {
      var paramsId = parseInt(key_id, 10);  // 10 Decimal
      let item = { 
        printerId: paramsId,
        printerName: this.state.printerName, 
        printerDescr: this.state.printerDescr,
        isInterval: this.state.isInterval, 
        printerIp: this.state.printerIp,
        printerPort: this.state.printerPort,
        isOnline: this.state.isOnline, 
        recCreated: this.state.recCreated, 
        recDatetime: this.state.recDatetime, 
      };
        //console.log(this.state.printerName);

          return <DataForm
            title="Edit Plants"
            hasCancel={true}
            item={item}
            bodyRender={this.fromBodyRender}
            onSubmit={itm => {
              // insert cca_plants
              var printerId = parseInt(itm.printerId);
              var printerPort = parseInt(itm.printerPort);
              var isOnline = ''+itm.isOnline;
              ApiService.editCcaPrinters(printerId,{
                printerId: printerId,
                printerName: itm.printerName,
                printerDescr: itm.printerDescr,
                printerIp: itm.printerIp,
                printerPort: printerPort,
                isOnline: isOnline,
                recCreated: itm.recCreated,
                recDatetime: itm.recDatetime
              })
              .then(res => {
                if(!res) throw new Error(res.status);
                else 
                  this.fromSubmit(itm);
                  this.props.history.push("/View");
                  alertMessage({message: 'Data '+itm.printerName+' update successfully.'})
                });
            }}
            onCancel={itm =>{
              alert("Data reset");
            }}
          />
    }
  }

  fromSubmit(itm) {
    
  }

  fromBodyRender(options) {
    const {item, binding} = options
    return <div className="row">
      <div className="col-md-8">
        <TextInput
        placeHolder="Printer Name"
            helpText= ""
            {...binding("printerName")}
          />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Printer Description"
          helpText= ""
          {...binding("printerDescr")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Printer IP"
          helpText= ""
          {...binding("printerIp")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Printer Port"
          helpText= ""
          {...binding("printerPort")}
        />
      </div>
      <div className="col-md-8">
        <CheckBox
          placeHolder="Is Online"
          {...binding("isOnline")}
        />
      </div>
    </div>
  }
}

export default EditForm
