import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

const TABS_DATA = []
var dateStr = "";
var date = new Date();

class AddForm extends BaseContainer {
  constructor(props) {
    super(props);
    this.fromBodyRender = this.fromBodyRender.bind(this);
    this.fromSubmit = this.fromSubmit.bind(this);
    this.state = {
      isLoading: true
    };
  }

  componentWillMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    //alert(dateStr)
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    const item = {
      printerName: "",
      printerDescr: "",
      printerIp: "",
      printerPort: 0,
      isOnline: "",
      recCreated: dateStr,
      recDatetime: dateStr
    }

    return <DataForm
        title="Add Printers"
        hasCancel={true}
        item={item}
        bodyRender={this.fromBodyRender}
        onSubmit={itm => {
          console.log(itm);
          // insert cca_printer
          var printerPort = parseInt(itm.printerPort);
          var isOnline = ''+itm.isOnline;
          ApiService.addCcaPrinters({
            printerName: itm.printerName,
            printerDescr: itm.printerDescr,
            printerIp: itm.printerIp,
            printerPort: printerPort,
            isOnline: isOnline,
            recCreated: itm.recCreated,
            recDatetime: itm.recDatetime
          })
          .then(res => {
            if(!res) throw new Error(res.status);
            else 
              this.fromSubmit(itm);
              alertMessage({message: 'Data '+itm.printerDescr+' added successfully.'})
          })
        }}
        onCancel={itm =>{

        }}
      />
  }

  fromSubmit(itm) {

  }

  fromBodyRender(options) {
    const {item, binding} = options
    return <div className="row">
      <div className="col-md-8">
        <TextInput
        placeHolder="Printer Name"
            helpText= ""
            {...binding("printerName")}
          />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Printer Description"
          helpText= ""
          {...binding("printerDescr")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Printer IP"
          helpText= ""
          {...binding("printerIp")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Printer Port"
          helpText= ""
          {...binding("printerPort")}
        />
      </div>
      <div className="col-md-8">
        <CheckBox
          placeHolder="Is Online"
          {...binding("isOnline")}
        />
      </div>
    </div>
  }
}

export default AddForm
