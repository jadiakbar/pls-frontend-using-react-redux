import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import Table from '../../../components/TablePallete'
import ApiService from '../../../service/ApiService'
import {alertMessage} from '../../../utils'
import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';
import Edit from '../components/EditForm';


//const TABLE_DATA = []
var data = [];

class TableView extends BaseContainer {
  state={
    header: [{
        title:"Seq Num",
        prop: "no"
    }, {
        title:"Production Order",
        prop:"workorderNo"
    }, {
        title:"Material",
        prop:"itemMaterialNo"
    }, {
        title:"Material Description",
        prop:"articleName"
    }, {
        title:"SSCC",
        prop:"ssccNumber"
    }, {
        title:"Batch",
        prop:"batchNo"
    }, {
        title:"Qty",
        prop:"itemQty"
    }, {
        title:"Best Before Date",
        prop:"bestBeforeDate"
    }, {
        title:"Prod. Date",
        prop:"prodDate"
    }, {
        title:"Scanned Date",
        prop:""
    }, {
        title:"Scanned By",
        prop:""
    }, {
        title:"Device",
        prop:""
    }, {
        title:"Printed By",
        prop:""
    }, {
        title:"Reprinted",
        prop:"isReprinted"
    }, {
        title:"Reprinted By",
        prop:""
    }, {
        title:"Reprinted Reason",
        prop:"reprintedReason"
    }, {
        title:"Blocked Stock",
        prop:"blockedStock"
    }],
    data: data
  }

  componentWillMount() {
    this.setState({
      title: "Pallet Report"
    })
    ApiService.fetchCcaPalletReportLites()
      .then(response => {
        this.setState({ 
          data: response.data 
        });
      })
      this.showLoading(true)
        setTimeout(() => {
          this.showLoading(false)
        }, 500);
  }


  _renderContent() {
    console.log(this.state)
    return <div className="portlet body">
      <Table
        searchableProperties={["workorderNo", "itemMaterialNo"]}
        hasTopBar={true}
        headers={this.state.header}
        data={this.state.data}
        rowRender={this._renderRow}
        onAddNewHandler={this.onAddNewClick.bind(this)}
      />
    </div>
  }

  onAddNewClick() {
    alert('Got to Add Data');
  }

  _renderRow(itm, i) {
    return <tr key={i} className="odd gradeX">
      <td>{i+1}</td>
      <td>{itm.workorderNo}</td>
      <td>{itm.itemMaterialNo}</td>
      <td>{itm.articleName}</td>
      <td>{itm.ssccNumber}</td>
      <td>{itm.batchNo}</td>
      <td>{itm.itemQty}</td>
      <td>{itm.bestBeforeDate}</td>
      <td>{itm.prodDate}</td>
      <td></td>
      <td></td>
      <td></td>
      <td>{itm.isReprinted}</td>
      <td></td>
      <td>{itm.reprintedReason}</td>
      <td>{itm.blockedStock}</td>
    </tr>
  }
}

export default TableView
