import React, { Component } from 'react';
import ApiService from '../../../service/ApiService'
import {alertMessage} from '../../../utils'
import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

var dateStr = "";
var date = new Date();
var key_id = 0;

export default class Edit extends Component {
  constructor(props) {
    super(props);
    this.onChangeRoleName = this.onChangeRoleName.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      roleId: '',
      roleName: ''
    }
  }

  componentDidMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);

    var url = window.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    key_id = idVal;

    ApiService.fetchMasterRolesById(key_id)
      .then(response => {
        this.setState({ 
          roleId: response.data.roleId,
          roleName: response.data.roleName
        });
      })
      .catch(function (error) {
        console.log(error);
      })
    }

  onChangeRoleName(e) {
    this.setState({
      roleName: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();
    var paramsId = parseInt(key_id, 10);  // 10 Decimal
    const obj = {
      roleId: paramsId,//this.props.match.params.id,
      roleName: this.state.roleName,
      recCreated: dateStr,
      recDatetime: dateStr
    };
    console.log(obj);
    ApiService.editMasterRoles(paramsId,obj)
    .then(res => {
      //this.props.history.push(View)
      this.props.history.push("/View")
    })
    alertMessage({message: 'Data added successfully.'})
  }
 
  render() {
    return (
        <div className="portlet light bordered">
          <div className="portlet-title">
            <Link to="/View" className="btn sbold blue" id="btnView">VIEW DATA<i className="fa fa-list-alt "></i></Link>
            <br />
            <div className="caption font-red-sunglo">
              <i className="icon-settings font-red-sunglo"></i><span className="caption-subject bold uppercase">Edit Printers</span>
            </div>
          </div>
          <div className="portlet-body form">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <div className="row">
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Role Name :  </label>
                      <input 
                        type="text" 
                        className="form-control" 
                        value={this.state.roleName}
                        onChange={this.onChangeRoleName}
                        />
                    </div>
                  </div>
                  <div className="col-sm-12">
                    <div className="form-actions noborder">
                      <input type="submit" 
                        value="Update" 
                        className="btn blue"/>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
    )
  }
}