import React, { Component } from 'react';
import ApiService from '../../../service/ApiService'
import {alertMessage} from '../../../utils'
import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

var dateStr = "";
var date = new Date();
var key_id = 0;

export default class EditForm extends Component {
  constructor(props) {
    super(props);
    this.onChangeUserName = this.onChangeUserName.bind(this);
    this.onChangeUserFullName = this.onChangeUserFullName.bind(this);
    this.onChangeUserPhone = this.onChangeUserPhone.bind(this);
    this.onChangeUserEmail = this.onChangeUserEmail.bind(this);
    this.onChangeUserStatus = this.onChangeUserStatus.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      userId: '',
      userName: '',
      userFullname:'',
      userPhone: '',
      userEmail: '',
      userStatus: '',
      recCreated: '',
      recDatetime: ''
    }
  }

  componentDidMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);

    var url = window.location.search;
    var urlParams = new URLSearchParams(url);
    var idVal = urlParams.get('id');
    key_id = idVal;

    ApiService.fetchMasterUsersById(key_id)
      .then(response => {
        this.setState({ 
          userId: response.data.userId,
          userName: response.data.userName, 
          userFullname: response.data.userFullname, 
          userPhone: response.data.userPhone,
          userEmail: response.data.userEmail,
          userStatus: response.data.userStatus
        });
      })
      .catch(function (error) {
        console.log(error);
      })
    }

  onChangeUserName(e) {
    this.setState({
      userName: e.target.value
    });
  }
  onChangeUserFullName(e) {
    this.setState({
      userFullname: e.target.value
    })  
  }
  onChangeUserPhone(e) {
    this.setState({
      userPhone: e.target.value
    })
  }
  onChangeUserEmail(e) {
    this.setState({
      userEmail: e.target.value
    })
  }
  onChangeUserStatus(e) {
    this.setState({
      userStatus: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();
    var paramsId = parseInt(key_id, 10);  // 10 Decimal
    const obj = {
      userId: paramsId,//this.props.match.params.id,
      userName: this.state.userName,
      userFullname: this.state.userFullname,
      userPhone: this.state.userPhone,
      userEmail: this.state.userEmail,
      userStatus: this.state.userStatus,
      recCreated: dateStr,
      recDatetime: dateStr
    };
    console.log(obj);
    ApiService.editMasterUsers(paramsId,obj)
    .then(res => {
      //this.props.history.push(View)
      this.props.history.push("/View")
    })
    alertMessage({message: 'Data added successfully.'})
  }
 
  render() {
    return (
        <div className="portlet light bordered">
          <div className="portlet-title">
            <Link to="/View" className="btn sbold blue" id="btnView">VIEW DATA<i className="fa fa-list-alt "></i></Link>
            <br />
            <div className="caption font-red-sunglo">
              <i className="icon-settings font-red-sunglo"></i><span className="caption-subject bold uppercase">Edit Plants</span>
            </div>
          </div>
          <div className="portlet-body form">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <div className="row">
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>User Name :  </label>
                      <input 
                        type="text" 
                        className="form-control" 
                        value={this.state.userName}
                        onChange={this.onChangeUserName}
                        />
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Full Name : </label>
                      <input type="text" 
                        className="form-control"
                        value={this.state.userFullname}
                        onChange={this.onChangeUserFullName}
                        />
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Phone : </label>
                      <input type="text" 
                        className="form-control"
                        value={this.state.userPhone}
                        onChange={this.onChangeUserPhone}
                        />
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Email : </label>
                      <input type="text" 
                        className="form-control"
                        value={this.state.userEmail}
                        onChange={this.onChangeUserEmail}
                        />
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Password : </label>
                      <input type="password" 
                        className="form-control"
                        value=""
                        onChange=""
                        />
                    </div>
                  </div>
                  <div className="col-xs-3 col-md-5">
                    <div className="form-group form-md-line-input">
                      <label>Confirm Password : </label>
                      <input type="password" 
                        className="form-control"
                        value=""
                        onChange=""
                        />
                    </div>
                  </div>
                  <div className="col-sm-12">
                    <div className="form-actions noborder">
                      <input type="submit" 
                        value="Update" 
                        className="btn blue"/>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
    )
  }
}