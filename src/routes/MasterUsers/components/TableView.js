import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import Table from '../../../components/Table'
import ApiService from '../../../service/ApiService'
import {alertMessage} from '../../../utils'
import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';
import Edit from '../components/EditForm';


//const TABLE_DATA = []
var data = [];

class TableView extends BaseContainer {
  state={
    header: [{
      title:"#",
      prop: "no"
    }, {
      title:"Username",
      prop: "userName"
    }, {
      title:"Full Name",
      prop: "userFullName"
    }, {
      title:"Phone",
      prop: "userPhone"
    }, {
      title:"Email",
      prop: "userEmail"
    }, {
      title:"Status",
      prop: "userStatus"
    }, {
        title:"Action"
      }],
      data: data
    }

  componentWillMount() {
    this.setState({
      title: "Users"
    })
    ApiService.fetchMasterUsers()
      .then(response => {
        this.setState({ 
          data: response.data 
        });
      })
      this.showLoading(true)
        setTimeout(() => {
          this.showLoading(false)
        }, 500);
  }


  _renderContent() {
    console.log(this.state)
    return <div className="portlet body">
      <Table
        searchableProperties={["userName", "userFullname","userPhone","userEmail"]}
        hasTopBar={true}
        headers={this.state.header}
        data={this.state.data}
        rowRender={this._renderRow}
        onAddNewHandler={this.onAddNewClick.bind(this)}
      />
    </div>
  }

  onAddNewClick() {

  }

  _renderRow(itm, i) {
    return <tr key={i} className="odd gradeX">
      <td>{i+1}</td>
      <td>{itm.userName}</td>
      <td>{itm.userFullName}</td>
      <td>{itm.userPhone}</td>
      <td>{itm.userEmail}</td>
      <td>
          {(() => {
            if (itm.userStatus) {
              return (
                <span className="badge badge-success">Active</span>
              )
            } else {
              return (
                <span className="badge badge-secondary">Off</span>
              )
            }
          })()}
      </td>
      <td>
        <div className="btn-group">
          <button className="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
            <i className="fa fa-angle-down"></i>
          </button>
          <ul className="dropdown-menu pull-left" role="menu">
            <li>
              <a href="javascript:;">
                <i className="icon-flag"></i> Area Name
                <span className="badge badge-success">4</span>
              </a>
            </li>
            <li>
              <a href="javascript:;">
                <i className="glyphicon glyphicon-tags"></i> Area Code </a>
            </li>
            <li className="divider"> </li>
            <li>
              <Link to={'/Edit/Find?id='+itm.userId } id="btnEdit"><i className="glyphicon glyphicon-pencil"></i> Edit </Link>
            </li>
            <li>
              <a onClick={e => handleClick(itm.userId)} >
                <i className="glyphicon glyphicon-trash"></i> Delete </a>
            </li>
          </ul>
        </div>
      </td>
    </tr>
  }
}

function handleClick(val) {
  var result = confirm("Are you sure...?");
  if (result) {
    ApiService.deleteMasterUsers(val)
    .then(response => {
      alertMessage({message: 'Data '+ response.data.userFullname +' deleted successfully'})
    })
  }
}

export default TableView
