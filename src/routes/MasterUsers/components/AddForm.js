import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

const TABS_DATA = []
var dateStr = "";
var date = new Date();
var plant_id = "";

class AddForm extends BaseContainer {
  handleSubmitted = ({ res, fields, form }) => {
    form.reset() // resets view
  }


  componentWillMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    //alert(dateStr)
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    const item = {
      userName: "",
      userFullname: "",
      userPhone: "",
      userEmail: "",
      userStatus: "1",
      recCreated: dateStr,
      recDatetime: dateStr
    }

    return <DataForm
        title="Add Plants"
        hasCancel={true}
        item={item}
        bodyRender={this.fromBodyRender}
        onSubmit={itm => {
          ApiService.fetchMasterUsers(itm)
            .then(res => {
              //this.props.history.push(View)
            });
        }}
        onCancel={itm =>{
          alert("Data reset");
        }}
      />
  }

  fromBodyRender(options) {
    const {item, binding} = options
    return <div className="row">
      <div className="col-xs-3 col-md-5">
        <TextInput
        placeHolder="Username"
            helpText= "Username"
            {...binding("userName")}
          />
      </div>
      <div className="col-xs-3 col-md-5">
        <TextInput
          placeHolder="FullName"
          helpText= "Full Name"
          {...binding("userFullname")}
        />
      </div>
      <div className="col-xs-3 col-md-5">
        <TextInput
          placeHolder="Phone"
          helpText= "Phone"
          {...binding("userPhone")}
        />
      </div>
      <div className="col-xs-3 col-md-5">
        <TextInput
          placeHolder="Email"
          helpText= "Email"
          {...binding("userEmail")}
        />
      </div>
      <div className="col-xs-3 col-md-5">
        <TextInput
          placeHolder="Password"
          helpText= "Password"
          {...binding("password")}
        />
      </div>
      <div className="col-xs-3 col-md-5">
        <TextInput
          placeHolder="Confirm Password"
          helpText= "Confirm Password"
          {...binding("confPassword")}
        />
      </div>
    </div>
  }
}

export default AddForm
