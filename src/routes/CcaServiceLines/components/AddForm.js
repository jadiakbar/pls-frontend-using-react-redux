import React from 'react'
import BaseContainer from '../../../containers/BaseContainer'
import ApiService from '../../../service/ApiService'
import {Tab, TextInput, CheckBox, RadioGroup, Select} from '../../../components/UI'
import DataForm from '../../../components/DataForm'
import {alertMessage} from '../../../utils'

import { BrowserRouter, Route, Switch, Link, useRouteMatch } from 'react-router-dom';

const TABS_DATA = []
var dateStr = "";
var date = new Date();

class AddForm extends BaseContainer {
  handleSubmitted = ({ res, fields, form }) => {
    form.reset() // resets view
  }

  componentWillMount() {
    dateStr =
    date.getFullYear() + "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2) + "T" +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
  }

  //--------------------------------------------------------------------//
  //  Form Content Renderer
  //--------------------------------------------------------------------//
  _renderContent() {
    const item = {
      scannerName: "",
      scannerIp: "",
      scannerPort: "",
      recCreated: dateStr,
      recDatetime: dateStr
    }

    return <DataForm
        title="Add Scanners"
        hasCancel={true}
        item={item}
        bodyRender={this.fromBodyRender}
        onSubmit={itm => {
          ApiService.addCcaScanners(itm)
            .then(res => {
              //this.props.history.push(View)
            })
            alertMessage({message: 'Data added successfully.'})
        }}
        onCancel={itm =>{

        }}
      />
  }

  fromBodyRender(options) {
    const {item, binding} = options
    return <div className="row">
      <div className="col-md-8">
        <TextInput
        placeHolder="scanner Name"
            helpText= "scanner Name"
            {...binding("scannerName")}
          />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Scanner IP"
          helpText= "Scanner IP"
          {...binding("scannerIp")}
        />
      </div>
      <div className="col-md-8">
        <TextInput
          placeHolder="Scanner Port"
          helpText= "Scanner Port"
          {...binding("scannerPort")}
        />
      </div>
      <div className="col-md-8">
        <Select
          placeHolder="Line"
          options={[
            {title:"Line 1", value: "L1"},
            {title:"Line 2", value: "L2"},
            {title:"Line 3", value: "L3"}]}
          {...binding("line")}
        />
      </div>
    </div>
  }
}

export default AddForm
