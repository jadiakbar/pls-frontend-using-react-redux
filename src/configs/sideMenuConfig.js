/*
export default {
  items : [{
    groupTitle: "Administration",
    items: [{
          title: "Plant Area",
          route: "/",
          icon: "icon-map"
      }, {
          title: "Line",
          route: "/tabs",
          icon: "icon-map"
      }, {
          title: "Plant Area",
          route: "/table",
          icon: "icon-map"
      }]
  }]
}
*/


export default {
  "items4":[
      {
         "groupTitle":"PLS",
         "items4":[
            {
               "title":"PLS - Labelling Operation",
               "route":"/lpa"
            }
         ]
      }
   ],
  "items3":[
      {
         "groupTitle":"Pallete Report",
         "items3":[
            {
               "title":"Pallete Report",
               "route":"/palletenquiry"
            }
         ]
      }
   ],
   "items2":[
      {
         "groupTitle":"Utilities",
         "items2":[
            {
               "title":"Manual Printing",
               "route":"/manual_printing"
            },
            {
               "title":"Partial Pallete Printing",
               "route":"/cca_part_pallet"
            },
            {
               "title":"Repack Printing",
               "route":"/repack_printing"
            },
            {
               "title":"Rework Printing",
               "route":"/rework_printing"
            }
         ]
      }
   ],
   "items":[
      {
         "groupTitle":"Administration",
         "items":[
            {
               "title":"Plant Area",
               "route":"/cca_plants"
            },
            {
               "title":"Lines",
               "route":"/cca_lines"
            },
            {
               "title":"Printers",
               "route":"/cca_printers"
            },
            {
               "title":"SICK Scanners",
               "route":"/cca_scanners"
            },
            {
               "title":"Material",
               "route":"/cca_items"
            },
            {
               "title":"Process Order",
               "route":"/cca_process_orders"
            },
            /* {
               "title":"Role",
               "route":"/master_roles"
            },
            {
               "title":"Users",
               "route":"/master_users"
            }, */
            {
               "title":"Mobile Device",
               "route":"/cca_devices"
            },
            {
               "title":"Setting",
               "route":"/cca_settings"
            },
            {
               "title":"Listener Service",
               "route":"/cca_utilities"
            },
            {
               "title":"Home",
               "route":"/"
            },
            {
               "title":"Tabs & UI Comps",
               "route":"/tabs"
            },
            {
               "title":"Table",
               "route":"/table"
            },
            {
               "title":"Counter",
               "route":"/counter"
            },
            {
               "title":"Blank",
               "route":"/blank_temp"
            },
            {
               "title":"Sample CRUD",
               "route":"/sample_crud"
            }
         ]
      }
   ]
}
