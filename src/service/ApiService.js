import axios from 'axios';

//const CORS_PROXY = 'https://cors-anywhere.herokuapp.com';
//const API_BASE_URL = CORS_PROXY + '/' +'http://localhost:5000/api';
//const API_BASE_URL = 'http://localhost:5000/api';
const API_BASE_URL = '/proxy/api';
// const idVal = urlParams.get('id');
/* if(idVal) {
    alert(idVal);
} else {
    //alert('id tidak ditemukan...!');
    idVal = 1;
} */



class ApiService {
//############## CcaPlants
    fetchCcaPlants() {
        return axios.get(API_BASE_URL + '/CcaPlants');
    }

    fetchCcaPlantsById(Id) {
        return axios.get(API_BASE_URL + '/CcaPlants' + '/' + Id);
    }

    deleteCcaPlants(Id) {
        return axios.delete(API_BASE_URL + '/CcaPlants' + '/' + Id);
    }

    addCcaPlants(data) {
        return axios.post(""+API_BASE_URL + '/CcaPlants', data);
    }

    editCcaPlants(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaPlants'+ '/' + Id, data);
    }

    fetchCcaPlantsLast() {
        return axios.get(API_BASE_URL + '/CcaPlants/Last');
    }

    fetchGetCcaPlantbyId(Id) {
        return axios.get(API_BASE_URL + '/CcaPlants' + '/GetCcaPlantbyId/' + Id);
    }


//############## CcaPlantRealtimes for CcaPlants adding
    fetchCcaPlantRealtimes() {
        return axios.get(API_BASE_URL + '/CcaPlantRealtimes');
    }

    fetchCcaPlantRealtimesById(Id) {
        return axios.get(API_BASE_URL + '/CcaPlantRealtimes' + '/' + Id);
    }

    deleteCcaPlantRealtimes(Id) {
        return axios.delete(API_BASE_URL + '/CcaPlantRealtimes' + '/' + Id);
    }

    addCcaPlantRealtimes(data) {
        return axios.post(""+API_BASE_URL + '/CcaPlantRealtimes', data);
    }

    editCcaPlantRealtimes(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaPlantRealtimes'+ '/' + Id, data);
    }


//############## CcaPlantWms 
    fetchCcaPlantWms() {
        return axios.get(API_BASE_URL + '/CcaPlantWms');
    }

    fetchCcaPlantWmsById(Id) {
        return axios.get(API_BASE_URL + '/CcaPlantWms' + '/' + Id);
    }

    deleteCcaPlantWms(Id) {
        return axios.delete(API_BASE_URL + '/CcaPlantWms' + '/' + Id);
    }

    addCCcaPlantWms(data) {
        return axios.post(""+API_BASE_URL + '/CcaPlantWms', data);
    }

    editCcaPlantWms(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaPlantWms'+ '/' + Id, data);
    }


//############## CcaPlantEwms 
    fetchCcaPlantEwms() {
        return axios.get(API_BASE_URL + '/CcaPlantEwms');
    }

    fetchCcaPlantEwmsById(Id) {
        return axios.get(API_BASE_URL + '/CcaPlantEwms' + '/' + Id);
    }

    deleteCcaPlantEwms(Id) {
        return axios.delete(API_BASE_URL + '/CcaPlantEwms' + '/' + Id);
    }

    addCcaPlantEwms(data) {
        return axios.post(""+API_BASE_URL + '/CcaPlantEwms', data);
    }

    editCcaPlantEwms(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaPlantEwms'+ '/' + Id, data);
    }



//############## MasterRolePlant for CcaPlants adding
    fetchMasterRolePlants() {
        return axios.get(API_BASE_URL + '/MasterRolePlants');
    }

    fetchMasterRolePlantsById(Id) {
        return axios.get(API_BASE_URL + '/MasterRolePlants' + '/' + Id);
    }

    deleteMasterRolePlants(Id) {
        return axios.delete(API_BASE_URL + '/MasterRolePlants' + '/' + Id);
    }

    addMasterRolePlants(data) {
        return axios.post(""+API_BASE_URL + '/MasterRolePlants', data);
    }

    editMasterRolePlants(Id,data) {
        return axios.put(API_BASE_URL  + '/MasterRolePlants'+ '/' + Id, data);
    }

    


//############## CcaLines
    fetchCcaLines() {
        return axios.get(API_BASE_URL + '/CcaLines');
    }

    fetchCcaLinesById(Id) {
        return axios.get(API_BASE_URL + '/CcaLines' + '/' + Id);
    }

    deleteCcaLines(Id) {
        return axios.delete(API_BASE_URL + '/CcaLines' + '/' + Id);
    }

    addCcaLines(data) {
        return axios.post(""+API_BASE_URL + '/CcaLines', data);
    }

    editCcaLines(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaLines'+ '/' + Id, data);
    }

    fetchCcaLinesLast() {
        return axios.get(API_BASE_URL + '/CcaLines/Last');
    }



//############## CcaPlantLines
    fetchCcaPlantLines() {
        return axios.get(API_BASE_URL + '/CcaPlantLines');
    }

    fetchCcaPlantLinesById(Id) {
        return axios.get(API_BASE_URL + '/CcaPlantLines' + '/' + Id);
    }

    deleteCcaPlantLines(Id) {
        return axios.delete(API_BASE_URL + '/CcaPlantLines' + '/' + Id);
    }

    addCcaPlantLines(data) {
        return axios.post(""+API_BASE_URL + '/CcaPlantLines', data);
    }

    editCcaPlantLines(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaPlantLines'+ '/' + Id, data);
    }


//############## CcaLineRepacks
    fetchCcaLineRepacks() {
        return axios.get(API_BASE_URL + '/CcaLineRepacks');
    }

    fetchCcaLineRepacksById(Id) {
        return axios.get(API_BASE_URL + '/CcaLineRepacks' + '/' + Id);
    }

    deleteCcaLineRepacks(Id) {
        return axios.delete(API_BASE_URL + '/CcaLineRepacks' + '/' + Id);
    }

    addCcaLineRepacks(data) {
        return axios.post(""+API_BASE_URL + '/CcaLineRepacks', data);
    }

    editCcaLineRepacks(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaLineRepacks'+ '/' + Id, data);
    }



//############## CcaLinePrinters
    fetchCcaLinePrinters() {
        return axios.get(API_BASE_URL + '/CcaLinePrinters');
    }

    fetchCcaLinePrintersById(Id) {
        return axios.get(API_BASE_URL + '/CcaLinePrinters' + '/' + Id);
    }

    deleteCcaLinePrinters(Id) {
        return axios.delete(API_BASE_URL + '/CcaLinePrinters' + '/' + Id);
    }

    addCcaLinePrinters(data) {
        return axios.post(""+API_BASE_URL + '/CcaLinePrinters', data);
    }

    editCcaLinePrinters(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaLinePrinters'+ '/' + Id, data);
    }




//############## CcaLinesScanners
    fetchCcaLineScanners() {
        return axios.get(API_BASE_URL + '/CcaLineScanners');
    }

    fetchCcaLineScannersById(Id) {
        return axios.get(API_BASE_URL + '/CcaLineScanners' + '/' + Id);
    }

    deleteCcaLineScanners(Id) {
        return axios.delete(API_BASE_URL + '/CcaLineScanners' + '/' + Id);
    }

    addCcaLineScanners(data) {
        return axios.post(""+API_BASE_URL + '/CcaLineScanners', data);
    }

    editCcaLineScanners(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaLineScanners'+ '/' + Id, data);
    }

    deleteCcaSickLineScanners(Id) {
        return axios.delete(API_BASE_URL + '/CcaLineScanners' + '/SickScanner/' + Id);
    }


//############## CcaPrinters
    fetchCcaPrinters() {
        return axios.get(API_BASE_URL + '/CcaPrinters');
    }

    fetchCcaPrintersById(Id) {
        return axios.get(API_BASE_URL + '/CcaPrinters' + '/' + Id);
    }

    deleteCcaPrinters(Id) {
        return axios.delete(API_BASE_URL + '/CcaPrinters' + '/' + Id);
    }

    addCcaPrinters(data) {
        return axios.post(""+API_BASE_URL + '/CcaPrinters', data);
    }

    editCcaPrinters(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaPrinters'+ '/' + Id, data);
    }

    fetchCcaOnlinePrinters() {
        return axios.get(API_BASE_URL + '/CcaOnlinePrinters');
    }

    fetchCcaOfflinePrinters() {
        return axios.get(API_BASE_URL + '/CcaOfflinePrinters');
    }


//############## CcaScanners
    fetchCcaScanners() {
        return axios.get(API_BASE_URL + '/CcaScanners');
    }

    fetchCcaScannersById(Id) {
        return axios.get(API_BASE_URL + '/CcaScanners' + '/' + Id);
    }

    deleteCcaScanners(Id) {
        return axios.delete(API_BASE_URL + '/CcaScanners' + '/' + Id);
    }

    addCcaScanners(data) {
        return axios.post(""+API_BASE_URL + '/CcaScanners', data);
    }

    editCcaScanners(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaScanners'+ '/' + Id, data);
    }

    fetchCcaScannerLast() {
        return axios.get(API_BASE_URL  + '/CcaScanners'+ '/Last');
    }


//############## CcaItems or Materials
    fetchCcaItems() {
        return axios.get(API_BASE_URL + '/CcaItems');
    }

    fetchCcaItemsById(Id) {
        return axios.get(API_BASE_URL + '/CcaItems' + '/' + Id);
    }

    deleteCcaItems(Id) {
        return axios.delete(API_BASE_URL + '/CcaItems' + '/' + Id);
    }

    addCcaItems(data) {
        return axios.post(""+API_BASE_URL + '/CcaItems', data);
    }

    editCcaItems(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaItems'+ '/' + Id, data);
    }


//############## CcaProcessOrders or CcaWorkorders on API
    fetchCcaProcessOrders() {
        return axios.get(API_BASE_URL + '/CcaWorkorders');
    }

    fetchCcaProcessOrdersById(Id) {
        return axios.get(API_BASE_URL + '/CcaWorkorders' + '/' + Id);
    }

    deleteCcaProcessOrders(Id) {
        return axios.delete(API_BASE_URL + '/CcaWorkorders' + '/' + Id);
    }

    addCcaProcessOrders(data) {
        return axios.post(""+API_BASE_URL + '/CcaWorkorders', data);
    }

    editCcaProcessOrders(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaWorkorders'+ '/' + Id, data);
    }


//############## MasterRoles on API
    fetchMasterRoles() {
        return axios.get(API_BASE_URL + '/MasterRoles');
    }

    fetchMasterRolesById(Id) {
        return axios.get(API_BASE_URL + '/MasterRoles' + '/' + Id);
    }

    deleteMasterRoles(Id) {
        return axios.delete(API_BASE_URL + '/MasterRoles' + '/' + Id);
    }

    addMasterRoles(data) {
        return axios.post(""+API_BASE_URL + '/MasterRoles', data);
    }

    editMasterRoles(Id,data) {
        return axios.put(API_BASE_URL  + '/MasterRoles'+ '/' + Id, data);
    }


//############## MasterUsers on API
    fetchMasterUsers() {
        return axios.get(API_BASE_URL + '/MasterUsers');
    }

    fetchMasterUsersById(Id) {
        return axios.get(API_BASE_URL + '/MasterUsers' + '/' + Id);
    }

    deleteMasterUsers(Id) {
        return axios.delete(API_BASE_URL + '/MasterUsers' + '/' + Id);
    }

    addMasterUsers(data) {
        return axios.post(""+API_BASE_URL + '/MasterUsers', data);
    }

    editMasterUsers(Id,data) {
        return axios.put(API_BASE_URL  + '/MasterUsers'+ '/' + Id, data);
    }


//############## CcaDevices on API
    fetchCcaDevices() {
        return axios.get(API_BASE_URL + '/CcaDevices');
    }

    fetchCcaDevicesById(Id) {
        return axios.get(API_BASE_URL + '/CcaDevices' + '/' + Id);
    }

    deleteCcaDevices(Id) {
        return axios.delete(API_BASE_URL + '/CcaDevices' + '/' + Id);
    }

    addCcaDevices(data) {
        return axios.post(""+API_BASE_URL + '/CcaDevices', data);
    }

    editCcaDevices(data) {
        return axios.put(API_BASE_URL  + '/CcaDevices'+ '/' + data.id, data);
    }


//############## CcaSettings on API
    fetchCcaSettings() {
        return axios.get(API_BASE_URL + '/CcaSettings');
    }

    fetchCcaSettingsById(Id) {
        return axios.get(API_BASE_URL + '/CcaSettings' + '/' + Id);
    }

    deleteCcaSettings(Id) {
        return axios.delete(API_BASE_URL + '/CcaSettings' + '/' + Id);
    }

    addCcaSettings(data) {
        return axios.post(""+API_BASE_URL + '/CcaSettings', data);
    }

    editCcaSettings(data) {
        return axios.put(API_BASE_URL  + '/CcaSettings'+ '/' + data.id, data);
    }


//############## CcaServiceLines or Listening Service on API
    fetchCcaServiceLines() {
        return axios.get(API_BASE_URL + '/CcaServiceLines');
    }

    fetchCcaServiceLinesById(Id) {
        return axios.get(API_BASE_URL + '/CcaServiceLines' + '/' + Id);
    }

    deleteCcaServiceLines(Id) {
        return axios.delete(API_BASE_URL + '/CcaServiceLines' + '/' + Id);
    }

    addCcaServiceLines(data) {
        return axios.post(""+API_BASE_URL + '/CcaServiceLines', data);
    }

    editCcaServiceLines(data) {
        return axios.put(API_BASE_URL  + '/CcaServiceLines'+ '/' + data.id, data);
    }


//############## CcaPalletReportLites or Listening Service on API
    fetchCcaPalletReportLites() {
        return axios.get(API_BASE_URL + '/CcaPalletReportLites');
    }

    fetchCcaPalletReportLitesById(Id) {
        return axios.get(API_BASE_URL + '/CcaPalletReportLites' + '/' + Id);
    }

    deleteCcaPalletReportLites(Id) {
        return axios.delete(API_BASE_URL + '/CcaPalletReportLites' + '/' + Id);
    }

    addCcaCcaPalletReportLites(data) {
        return axios.post(""+API_BASE_URL + '/CcaPalletReportLites', data);
    }

    editCcaPalletReportLites(Id,data) {
        return axios.put(API_BASE_URL  + '/CcaPalletReportLites'+ '/' + Id, data);
    }



}

export default new ApiService();